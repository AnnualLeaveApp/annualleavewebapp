package com.AnnualLeaveApp.demo.AnnualLeaveApp.domain.services.provider;


import com.AnnualLeaveApp.demo.AnnualLeaveApp.data.model.Role;
import com.AnnualLeaveApp.demo.AnnualLeaveApp.data.repo.RoleRepo;
import com.AnnualLeaveApp.demo.AnnualLeaveApp.domain.provider.CrudProvider;

import java.util.UUID;

public interface RoleProvider extends CrudProvider<Role, UUID, RoleRepo> {

  Role findRoleByName(String name);

}
