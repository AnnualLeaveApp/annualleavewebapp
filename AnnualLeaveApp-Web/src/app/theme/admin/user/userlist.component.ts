import { ChangeDetectionStrategy, ChangeDetectorRef, Component, OnInit, ViewChild, ViewEncapsulation, Output, EventEmitter, ElementRef } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { animate, style, transition, trigger } from '@angular/animations';
import swal from 'sweetalert2';
import { FormBuilder, FormGroup, Validators, FormArray, FormControl, ValidatorFn } from '@angular/forms';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/filter';
import { FormService } from 'src/app/services/validators/services';
import { UserService, RoleService } from '../../../../api/services';
import { MatPaginator, MatSort } from '@angular/material';
import { Role, User, Contract, UserRequest } from '../../../../api/models';
import { UserTableDataSource } from 'src/app/services/datatables/user.datatable.service';
import { CustomValidators } from 'src/app/services/validators/modal.custom.validators';
import { IMyDpOptions } from 'mydatepicker';
import { Helper } from 'src/app/helpers/helper';
import { fromEvent } from 'rxjs';



@Component({
  selector: 'app-user-list',
  templateUrl: './userlist.component.html',
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush,
  styleUrls: [
    './userlist.component.scss',
    '../../../../assets/icon/icofont/css/icofont.scss'
  ],
  animations: [
    trigger('fadeInOutTranslate', [
      transition(':enter', [
        style({ opacity: 0 }),
        animate('400ms ease-in-out', style({ opacity: 1 }))
      ]),
      transition(':leave', [
        style({ transform: 'translate(0)' }),
        animate('400ms ease-in-out', style({ opacity: 0 }))
      ])
    ])
  ]
})
export class UserlistComponent implements OnInit {
  /** >> MATERIAL DATA TABLE */
  displayedColumns = ["firstName", "lastName", "username", "email", "actions"];
  dataSource: any | null;
  index: number;
  id: number;

  @ViewChild(MatPaginator, null) paginator: MatPaginator;
  @ViewChild(MatSort, null) sort: MatSort;
  @ViewChild('filter') filter: ElementRef;


  /** << MATERIAL DATA TABLE */

  // general entity types memebers
  public userList: User[];
  public currentUser: User;
  public activeUsersList: User[] = [];
  public activeRoles: Role[] = [];
  public userAssignedRoles: Role[] = [];
  public contractList: Contract[] = [];
  private pageSize = 10;
  private pageIndex = 1;
  public saveBtn = "Save";

  // selected form entity value
  public selectedRoles: Role[] = [];
  public selectedUser: User;
  public selectedUserContract: Contract;

  private initDate = new Date();
  public date: any = { date: { year: (this.initDate).getFullYear(), month: (this.initDate).getMonth() + 1, day: (this.initDate).getDate() } }
  public startDate: any = { date: { year: (this.initDate).getFullYear(), month: (this.initDate).getMonth() + 1, day: (this.initDate).getDate() } }
  public endDate: any = { date: { year: (this.initDate).getFullYear(), month: (this.initDate).getMonth() + 1, day: (this.initDate).getDate() } }
  public startDatePickerModel: any = { date: { year: this.initDate.getFullYear(), month: this.initDate.getMonth(), day: this.initDate.getDate() } };
  public endDatePickerModel: any = { date: { year: this.initDate.getFullYear(), month: this.initDate.getMonth(), day: this.initDate.getDate() } };

  //modal & form variables
  public startDatePickerOptions: IMyDpOptions =
    {
      dateFormat: 'dd/mm/yyyy',
      minYear: this.initDate.getFullYear() - 1,
      maxYear: this.initDate.getFullYear() + 1,
      markCurrentDay: true,
    };
  public endDatePickerOptions: IMyDpOptions =
    {
      dateFormat: 'dd/mm/yyyy',
      minYear: this.initDate.getFullYear(),
      maxYear: this.initDate.getFullYear() + 1,
      markCurrentDay: true
    };

  //modal & form variables
  userModalForm: FormGroup;
  public userModalFormErrors = {
    user_firstName: '',
    user_lastName: '',
    user_usernameInput: '',
    user_emailInput: '',
    user_contract_startDateInput: '',
    user_contract_endDateInput: '',
    user_contract_annualLeaveDaysInput: ''
  };
  loadingModal = false;
  submitted = false;
  public isFormReady = false;
  isCreateOperation = false;
  isUpdateOperation = false;

  constructor(private fb: FormBuilder, private chRef: ChangeDetectorRef, public FormService: FormService,
    public http: HttpClient, private roleService: RoleService, private userService: UserService,
    private helper: Helper) { }

  ngOnInit() {
    this.initializeUserData();
    this.loadData();
    this.fetchRoles();
  }

  //#region Initialization Operations

  initializeUserData() {
    this.userService.findAllUsers().subscribe(
      response => {
        this.userList = response as User[];
      },
      err => {
        swal(
          'System Error',
          'Please provide this error to the IT Department!',
          'error'
        );
      }
    );
  }

  initializeUserModal() {

    const roleControls = this.userAssignedRoles.map(c => new FormControl(false));
    for (var i = 0; i < this.userAssignedRoles.length; i++) {
      if (this.selectedUser && this.selectedUser.roles.filter(item => item == this.userAssignedRoles[i].id).length > 0) {
        roleControls[i].setValue(true);
      }
    }

    this.userModalForm = this.fb.group({
      user_usernameInput:
        [
          '',
          Validators.required
        ],
      user_emailInput:
        [
          '',
          [Validators.required, CustomValidators.ValidateEmail]
        ],
      user_firstNameInput:
        [
          '',
          Validators.required
        ],
      user_lastNameInput:
        [
          '',
          Validators.required
        ],
      user_contract_startDateInput:
        [
          '',
          [Validators.required, CustomValidators.ValidContractDateRanges]
        ],
      user_contract_endDateInput:
        [
          '',
          [Validators.required, CustomValidators.ValidContractDateRanges]
        ],
      user_contract_annualLeaveDaysInput:
        [
          '',
          Validators.required
        ],
      userAssignedRoles: new FormArray(roleControls, this.minSelectedCheckboxes(1)),
    });

    this.userModalForm.valueChanges.subscribe((data) => {
      this.userModalFormErrors = this.FormService.validateForm(this.userModalForm, this.userModalFormErrors, true)
    });

    this.isFormReady = true;
  }

  //#endregion

  //#region CRUD Operations over user

  fetchRoles() {
    this.roleService.findAllRoles().subscribe(
      response => {
        this.activeRoles = response as Role[];
        this.userAssignedRoles = this.activeRoles;
        this.initializeUserModal();
      },
      err => {
        swal('System Error',
          'Please provide this error to the IT Department!',
          'error'
        )
      }
    );
  }

  getUserContract(userId: string) {
    this.userService.findContractOfUser(userId).subscribe(
      response => {
        this.selectedUserContract = response;
        if (this.isUpdateOperation) {
          this.setUserFormValuesOnEventUpdate();
          this.openMyModal('UserModal');
        }
      },
      err => {
        swal('System Error',
          'Please provide this error to the IT Department!',
          'error'
        )
      }
    );
  }

  makeCreationCall(modalFormValues: any) {
    let postBody: UserRequest = {
      user: {
        person: {
          firstName: modalFormValues.user_firstNameInput,
          lastName: modalFormValues.user_lastNameInput
        },
        username: modalFormValues.user_usernameInput,
        email: modalFormValues.user_emailInput,
        roles: [
          this.getRolesOnCreate_Update(modalFormValues).toString()
        ]
      },
      contract: {
        validFrom: this.helper.formatDateString(modalFormValues.user_contract_startDateInput.date),
        validTo: modalFormValues.user_contract_endDateInput ? this.helper.formatDateString(modalFormValues.user_contract_endDateInput.date) : null,
        annualLeaveDays: modalFormValues.user_contract_annualLeaveDaysInput != null ? +modalFormValues.user_contract_annualLeaveDaysInput : 0
      }
    }

    this.userService.createUser(postBody).subscribe(
      (data: any) => {
        if (data) {
          this.loadingModal = false;
          this.refresh();
          swal(
            "User created!",
            "The record has been successfully saved!",
            'success'
          );
          this.closeMyModal('UserModal');
        }
      },
      error => {
        this.loadingModal = false;
        this.closeMyModal('UserModal');
      });
  }

  makeUpdateCall(modalFormValues: any) {
    let userRequest: UserRequest = {
      user: {
        person: {
          firstName: modalFormValues.user_firstNameInput,
          lastName: modalFormValues.user_lastNameInput
        },
        username: modalFormValues.user_usernameInput,
        email: modalFormValues.user_emailInput,
        roles: [
          this.getRolesOnCreate_Update(modalFormValues).toString()
        ]
      },
      contract: {
        validFrom: this.helper.formatDateString(modalFormValues.user_contract_startDateInput.date),
        validTo: modalFormValues.user_contract_endDateInput ? this.helper.formatDateString(modalFormValues.user_contract_endDateInput.date) : null,
        annualLeaveDays: modalFormValues.user_contract_annualLeaveDaysInput != null ? +modalFormValues.user_contract_annualLeaveDaysInput : 0
      }
    }
    let params: UserService.UpdateUserParams = {
      userId: this.selectedUser.id,
      userRequest: userRequest
    }

    this.userService.updateUser(params).subscribe(
      (data: any) => {
        if (data) {
          this.loadingModal = false;
          this.refresh();
          swal(
            "User updated!",
            "The record has been successfully updated!",
            'success'
          );
          this.closeMyModal('UserModal');
        }
      },
      error => {
        this.loadingModal = false;
        this.closeMyModal('UserModal');
      });
  }

  makeDeleteCall(selectedUserIDToDelete: string) {
    this.userService.deleteUser(selectedUserIDToDelete).subscribe(
      (data: any) => {
        if (data) {
          this.loadingModal = false;
          this.refresh();
          swal(
            "User deleted!",
            "The record has been successfully deleted!",
            'success'
          );
          this.closeMyModal('UserModal');
        }
      },
      error => {
        this.loadingModal = false;
        this.closeMyModal('UserModal');
      });
  }

  //#endregion

  //#region UI, Modal & Form Methods

  saveUser() {
    this.submitted = true;

    this.userModalForm.controls.user_contract_endDateInput.clearValidators();
    this.userModalForm.controls.user_contract_endDateInput.updateValueAndValidity();

    if (this.userModalForm.invalid) {
      this.userModalFormErrors = this.FormService.validateForm(this.userModalForm, this.userModalFormErrors, false)
      return;
    }
    this.submitted = false;

    const val = this.userModalForm.value;

    if (this.isCreateOperation) {
      this.makeCreationCall(val);
      this.isCreateOperation = !this.isCreateOperation;
    }

    if (this.isUpdateOperation) {
      this.makeUpdateCall(val);
      this.isUpdateOperation = !this.isUpdateOperation;
    }
  }

  openMyModal(modalID) {
    document.querySelector('#' + modalID).classList.add('md-show');
  }

  closeMyModal(modalID) {
    this.submitted = false;
    this.loadingModal = false;
    this.isUpdateOperation = false;
    this.isCreateOperation = false;
    this.saveBtn = "Save";
    if (modalID == "UserModal") {
      document.querySelector('#' + modalID).classList.remove('md-show');
    }
    else {
      ((modalID.target.parentElement.parentElement).parentElement).classList.remove('md-show');
    }
    this.userModalForm.reset();
  }


  setUserFormValuesOnEventUpdate(): void {

    let validFromDate = this.getFormatedDate(this.selectedUserContract.validFrom);
    let validToDate = null;
    if(this.selectedUserContract.validTo){
      validToDate = this.getFormatedDate(this.selectedUserContract.validTo);
    }

    this.userModalForm.patchValue({
      user_usernameInput: this.selectedUser.username,
      user_emailInput: this.selectedUser.email,
      user_firstNameInput: this.selectedUser.person.firstName,
      user_lastNameInput: this.selectedUser.person.lastName,
      user_contract_startDateInput: {
        date: {
          year: validFromDate.getFullYear(),
          month: validFromDate.getMonth() + 1,
          day: validFromDate.getDate()
        }
      },
      user_contract_endDateInput: validToDate !=null ? {
        date: {
          year: validToDate.getFullYear(),
          month: validToDate.getMonth() + 1,
          day: validToDate.getDate()
        }
      } : "",
      user_contract_annualLeaveDaysInput: this.selectedUserContract.annualLeaveDays
    }, { onlySelf: true, emitEvent: true });

    this.startDate = this.userModalForm.controls.user_contract_startDateInput.value;
    this.endDate = this.userModalForm.controls.user_contract_endDateInput.value;
  }

  //#endregion

  //#region Private Helper Methods

  public loadData() {

    this.dataSource = new UserTableDataSource(
      this.paginator,
      this.sort,
      this.userService
    );

    fromEvent(this.filter.nativeElement, 'keyup')
      // .debounceTime(150)
      // .distinctUntilChanged()
      .subscribe(() => {
        if (!this.dataSource) {
          return;
        }
        this.dataSource.filter = this.filter.nativeElement.value;
      });

  }

  //** >> MATERIAL DATA TABLE */
  refresh() {
    this.loadData();
  }

  handleEvent(action: string, userToUpdate?: User): void {
    switch (action) {
      case "Create":
        this.isCreateOperation = true;
        this.isUpdateOperation = false;
        this.openMyModal('UserModal');
        break;
      case "Update":
        this.isUpdateOperation = true;
        this.isCreateOperation = false;
        this.isUpdateOperation = true;
        this.selectedUser = userToUpdate;
        this.initializeUserModal();
        this.getUserContract(userToUpdate.id);
        break;
    }
  }

  deleteSelectedUser(userToDelete: User) {
    swal({
      title: 'Attention!',
      text: "Are you sure to delete user?",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete!',
      cancelButtonText: 'No!',
    }).then((makeDelete) => {
      if (makeDelete.value) {
        this.makeDeleteCall(userToDelete.id);
      }
    });
  }

  getRolesOnCreate_Update(values: any): string[] {
    let selectedRoleIDs: string[] = [];
    selectedRoleIDs = values.userAssignedRoles.map((v, i) => v ? this.userAssignedRoles[i].id : null)
      .filter(v => v !== null);
    return selectedRoleIDs;
  }

  minSelectedCheckboxes(min = 1) {
    const validator: ValidatorFn = (formArray: FormArray) => {
      const totalSelected = formArray.controls
        // get a list of checkbox values (boolean)
        .map(control => control.value)
        // total up the number of checked checkboxes
        .reduce((prev, next) => next ? prev + next : prev, 0);

      // if the total is not greater than the minimum, return the error message
      return totalSelected >= min ? null : { required: true };
    };

    return validator;
  }

  getFormatedDate(date) {
    return this.helper.formatDateTimeFromString(date);
  }

  //#endregion

}
