/* tslint:disable */
import { User } from './user';
import { Contract } from './contract';
export interface UserRequest {
  user?: User;
  contract?: Contract;
}
