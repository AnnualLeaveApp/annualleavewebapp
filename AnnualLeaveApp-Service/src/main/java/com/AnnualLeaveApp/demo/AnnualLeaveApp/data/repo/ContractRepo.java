package com.AnnualLeaveApp.demo.AnnualLeaveApp.data.repo;

import com.AnnualLeaveApp.demo.AnnualLeaveApp.data.model.Contract;
import com.AnnualLeaveApp.demo.AnnualLeaveApp.data.model.Person;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.sql.Date;
import java.time.LocalDate;
import java.util.Optional;
import java.util.UUID;

public interface ContractRepo extends JpaRepository<Contract, UUID> {
    @Query("select c from Contract c "
            + "where c.person = :person"
            + " and (c.activeTo is NULL"
            + " or c.activeTo >= :requestedDate"
            + " or c.activeFrom <= :requestedDate) ")
    Optional<Contract> findValidContractByPerson(Person person, Date requestedDate);

}
