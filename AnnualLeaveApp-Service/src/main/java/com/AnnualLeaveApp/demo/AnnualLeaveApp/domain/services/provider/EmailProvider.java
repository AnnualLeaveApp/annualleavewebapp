package com.AnnualLeaveApp.demo.AnnualLeaveApp.domain.services.provider;

import com.AnnualLeaveApp.demo.AnnualLeaveApp.data.model.MailTemplate;
import com.AnnualLeaveApp.demo.AnnualLeaveApp.data.model.SupervisorMail;
import com.AnnualLeaveApp.demo.AnnualLeaveApp.data.model.UserMail;
import org.springframework.mail.SimpleMailMessage;

import java.time.LocalDate;

public interface EmailProvider {

  void sendMail(SimpleMailMessage simpleMailMessage);

  SimpleMailMessage createUserNotificationMail(String mailTo, String mailFrom, MailTemplate mail, UserMail userMailData);

  String createUserNotificationMailBody(MailTemplate mail, UserMail userMailData);

  SimpleMailMessage createSupervisorNotificationMail(String mailTo, String mailFrom, MailTemplate mail, SupervisorMail supervisorMailData);

  String createSupervisorNotificationMailBody(MailTemplate mail, SupervisorMail supervisorMailData);
}
