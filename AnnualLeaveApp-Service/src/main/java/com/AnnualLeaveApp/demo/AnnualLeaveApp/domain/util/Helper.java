package com.AnnualLeaveApp.demo.AnnualLeaveApp.domain.util;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;

public class Helper {

    private Helper() {
    }
    private static class LazyInit {
        private static final Helper instance = new Helper();
    }
    static Helper createInstance() {
        return LazyInit.instance;
    }
    int i;

    public static int getTotalDaysBetweenDates(LocalDate startDate, LocalDate endDate){
        return (int) ChronoUnit.DAYS.between(startDate, endDate);
    }
}

