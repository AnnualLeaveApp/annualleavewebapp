package com.AnnualLeaveApp.demo.AnnualLeaveApp.data.model;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "leave_type", schema = "lhind_demo", catalog = "")
public class LeaveType {
    private int id;
    private String type;

    @Id
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "type")
    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
