package com.AnnualLeaveApp.demo.AnnualLeaveApp.domain.services.provider;

import com.AnnualLeaveApp.demo.AnnualLeaveApp.data.model.Application;
import com.AnnualLeaveApp.demo.AnnualLeaveApp.data.model.Person;
import com.AnnualLeaveApp.demo.AnnualLeaveApp.data.model.User;
import com.AnnualLeaveApp.demo.AnnualLeaveApp.data.model.UserToken;
import com.AnnualLeaveApp.demo.AnnualLeaveApp.data.repo.ApplicationRepo;
import com.AnnualLeaveApp.demo.AnnualLeaveApp.data.repo.UserRepo;
import com.AnnualLeaveApp.demo.AnnualLeaveApp.domain.provider.CrudProvider;

import java.util.Optional;
import java.util.UUID;

public interface UserProvider extends CrudProvider<User, UUID, UserRepo> {

    Optional<User> findByUsername(String username);

    UserToken login(User user);
    User findLoggedInUser(User user);
    int getRemainingAnnualLeaveDays(Person person);

}
