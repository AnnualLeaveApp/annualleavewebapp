package com.AnnualLeaveApp.demo.AnnualLeaveApp.domain.services.provider;

import com.AnnualLeaveApp.demo.AnnualLeaveApp.data.dto.swagger.RejectRequestDTO;
import com.AnnualLeaveApp.demo.AnnualLeaveApp.data.model.*;
import com.AnnualLeaveApp.demo.AnnualLeaveApp.data.repo.ApplicationRepo;
import com.AnnualLeaveApp.demo.AnnualLeaveApp.domain.provider.CrudProvider;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface ApplicationProvider extends CrudProvider<Application, UUID, ApplicationRepo> {

    List<Application> getAllPersonAnnualLeaveApplication(Person person);

    Optional<Application> approveApplication(UUID applicationId);

    Optional<Application> rejectApplication(UUID applicationId, RejectRequestDTO rejectRequestDTO);


}
