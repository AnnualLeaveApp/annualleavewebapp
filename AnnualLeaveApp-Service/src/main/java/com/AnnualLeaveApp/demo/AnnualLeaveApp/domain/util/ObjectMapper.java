package com.AnnualLeaveApp.demo.AnnualLeaveApp.domain.util;

import com.AnnualLeaveApp.demo.AnnualLeaveApp.data.dto.swagger.*;
import com.AnnualLeaveApp.demo.AnnualLeaveApp.data.model.*;

import javax.servlet.http.HttpServletRequest;
import java.sql.Date;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

public class ObjectMapper {

    public static Application map(ApplicationDTO applicationDTO) {
        Application application = new Application();
        application.setId(applicationDTO.getId());
        Person person = new Person();
        person.setFirstName(applicationDTO.getFirstName());
        person.setLastName(applicationDTO.getLastName());
        application.setPerson(person);
        application.setStartDate(applicationDTO.getStartDate());
        application.setEndDate(applicationDTO.getEndDate());
        LeaveType leaveType = new LeaveType();
        leaveType.setId(applicationDTO.getLeaveTypeId());
        application.setLeaveType(leaveType);
        ApplicationStatus status = new ApplicationStatus();
        status.setStatus(applicationDTO.getApplicationStatus());
        application.setApplicationStatus(status);
        return application;
    }

    public static ApplicationDTO map(Application application) {
        ApplicationDTO applicationDTO = new ApplicationDTO();
        applicationDTO.setId(application.getId());
        applicationDTO.setStartDate(application.getStartDate());
        applicationDTO.setEndDate(application.getEndDate());
        applicationDTO.setLeaveTypeId(application.getLeaveType().getId());
        applicationDTO.setApplicationStatus(application.getApplicationStatus() != null ? application.getApplicationStatus().getStatus(): null);
        applicationDTO.setFirstName(application.getPerson().getFirstName());
        applicationDTO.setLastName(application.getPerson().getLastName());
        return applicationDTO;
    }

    public static Contract map(ContractDTO contractDTO) {
        Contract contract = new Contract();
        contract.setId(contractDTO.getId());
        contract.setAnnualLeaveDays(contractDTO.getAnnualLeaveDays());
        contract.setActiveFrom(Date.valueOf(contractDTO.getValidFrom()));
        contract.setActiveTo(Date.valueOf(contractDTO.getValidTo()));
        return contract;
    }

    public static ContractDTO map(Contract contract) {
        ContractDTO contractDTO = new ContractDTO();
        contractDTO.setId(contract.getId());
        contractDTO.setAnnualLeaveDays(contract.getAnnualLeaveDays());
        contractDTO.setValidFrom(contract.getActiveFrom().toLocalDate());
        contractDTO.setValidTo(contract.getActiveTo() != null ? contract.getActiveTo().toLocalDate(): null);
        return contractDTO;
    }

    public static LeaveType map(LeaveTypeDTO leaveTypeDTO) {
        LeaveType leaveType = new LeaveType();
        leaveType.setId(leaveTypeDTO.getId());
        leaveType.setType(leaveTypeDTO.getType());
        return leaveType;
    }

    public static LeaveTypeDTO map(LeaveType leaveType) {
        LeaveTypeDTO leaveTypeDTO = new LeaveTypeDTO();
        leaveTypeDTO.setId(leaveType.getId());
        leaveTypeDTO.setType(leaveType.getType());
        return leaveTypeDTO;
    }

    public static Role map(RoleDTO roleDTO) {
        Role role = new Role();
        role.setId(roleDTO.getId());
        role.setName(roleDTO.getName());
        return role;
    }

    private static Role getRoleFromId(UUID roleId){
        Role role = new Role();
        role.setId(roleId);
        return role;
    }

    public static RoleDTO map(Role role) {
        RoleDTO roleDTO = new RoleDTO();
        roleDTO.setId(role.getId());
        roleDTO.setName(role.getName());
        return roleDTO;
    }

    public static User map(LoginDTO loginDTO) {
        User user = new User();
        user.setUsername(loginDTO.getUsername());
        user.setPassword(loginDTO.getPassword());
        return user;
    }

    public static Optional<User> map(Optional<HttpServletRequest> httpServletRequest) {
        com.fasterxml.jackson.databind.ObjectMapper objectMapper = new com.fasterxml.jackson.databind.ObjectMapper();
        if (!httpServletRequest.isPresent())
            return Optional.empty();
        return getUserByHttpRequest(httpServletRequest, objectMapper);
    }

    private static Optional<User> getUserByHttpRequest(Optional<HttpServletRequest> httpServletRequest, com.fasterxml.jackson.databind.ObjectMapper objectMapper) {
        return httpServletRequest.get().getAttribute("user") != null ? Optional.of((User) httpServletRequest.get().getAttribute("user")) : Optional.empty();
    }

    public static Person map(PersonDTO personDTO){
        Person person = new Person();
        person.setId(personDTO.getId());
        person.setFirstName(personDTO.getFirstName());
        person.setLastName(personDTO.getLastName());
        return person;
    }

    public static PersonDTO map(Person person){
        PersonDTO personDTO = new PersonDTO();
        personDTO.setId(person.getId());
        personDTO.setFirstName(person.getFirstName());
        personDTO.setLastName(person.getLastName());
        return personDTO;
    }

    public static UserDTO map(User user) {
        UserDTO userDTO = new UserDTO();
        userDTO.setId(user.getId());
        userDTO.setPerson(map(user.getPerson()));
        userDTO.setEmail(user.getEmail());
        userDTO.setToken(user.getVerificationToken());
        userDTO.setUsername(user.getUsername());
        userDTO.setRoles(user.getRoleList().stream().map(Role::getId).collect(Collectors.toList()));
        return userDTO;
    }

    public static User map(UserDTO userDTO) {
        User user = new User();
        user.setId(userDTO.getId());
        user.setUsername(userDTO.getUsername());
        user.setPerson(map(userDTO.getPerson()));
        user.setEmail(userDTO.getEmail());
        user.setRoleList(userDTO.getRoles().stream().map(id -> getRoleFromId(id)).collect(Collectors.toList()));
        return user;
    }

    public static UserDTO map(UserToken userToken) {
        UserDTO userDTO = new UserDTO();
        userDTO.setId(userToken.getId());
        userDTO.setUsername(userToken.getUsername());
        userDTO.setPerson(map(userToken.getPerson()));
        userDTO.setEmail(userToken.getEmail());
        userDTO.setToken(userToken.getToken());
        userDTO.setRoles(userToken.getRoleList().stream().map(Role::getId).collect(Collectors.toList()));
        return userDTO;
    }

}
