package com.AnnualLeaveApp.demo.AnnualLeaveApp.presentation.controller;

import com.AnnualLeaveApp.demo.AnnualLeaveApp.data.dto.swagger.ApplicationDTO;
import com.AnnualLeaveApp.demo.AnnualLeaveApp.data.dto.swagger.LoginDTO;
import com.AnnualLeaveApp.demo.AnnualLeaveApp.data.dto.swagger.RejectRequestDTO;
import com.AnnualLeaveApp.demo.AnnualLeaveApp.data.dto.swagger.UserDTO;
import com.AnnualLeaveApp.demo.AnnualLeaveApp.data.model.Application;
import com.AnnualLeaveApp.demo.AnnualLeaveApp.domain.services.provider.ApplicationProvider;
import com.AnnualLeaveApp.demo.AnnualLeaveApp.domain.services.provider.UserProvider;
import com.AnnualLeaveApp.demo.AnnualLeaveApp.domain.util.ObjectMapper;
import com.AnnualLeaveApp.demo.AnnualLeaveApp.presentation.swagger.ApplicationApi;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@RestController
@Controller
@RequestMapping("/api/v1")
public class ApplicationController implements ApplicationApi {

    @Autowired
    ApplicationProvider applicationProvider;

    @Autowired
    HttpServletRequest httpServletRequest;

    @Override
    public Optional<HttpServletRequest> getRequest() {
        return Optional.of(httpServletRequest);
    }

    @Override
    public ResponseEntity<ApplicationDTO> create(@Valid ApplicationDTO application) {
        return ResponseEntity.ok(ObjectMapper.map(applicationProvider.create(ObjectMapper.map(application), ObjectMapper.map(getRequest()))));
    }

    @Override

    public ResponseEntity<UUID> delete(UUID applicationId) {
        return ResponseEntity.ok(applicationProvider.delete(applicationId));
    }

    @Override
    public ResponseEntity<List<ApplicationDTO>> findAll() {
        return ResponseEntity.ok(
                applicationProvider
                        .findAll()
                        .stream()
                        .map(ObjectMapper::map)
                        .collect(Collectors.toList()));
    }

    @Override
    public ResponseEntity<List<ApplicationDTO>> findAllByUser() {
        return ResponseEntity.ok(
                applicationProvider
                        .getAllPersonAnnualLeaveApplication(ObjectMapper.map(getRequest()).get().getPerson())
                        .stream()
                        .map(ObjectMapper::map)
                        .collect(Collectors.toList()));
    }

    @Override
    public ResponseEntity<ApplicationDTO> findById(UUID applicationId) {
        return ResponseEntity.ok(ObjectMapper.map(applicationProvider.findById(applicationId).get()));
    }

    @Override
    public ResponseEntity<ApplicationDTO> update(@Valid ApplicationDTO application, UUID applicationId) {
        return ResponseEntity.ok(ObjectMapper.map(applicationProvider.update(ObjectMapper.map(application), applicationId, ObjectMapper.map(getRequest())).get()));
    }

    @Override
    public ResponseEntity<ApplicationDTO> approve(UUID applicationId) {
        return ResponseEntity.ok(ObjectMapper.map(applicationProvider.approveApplication(applicationId).get()));
    }

    @Override
    public ResponseEntity<ApplicationDTO> reject(UUID applicationId, @Valid RejectRequestDTO rejectRequest) {
        return ResponseEntity.ok(ObjectMapper.map(applicationProvider.rejectApplication(applicationId, rejectRequest).get()));
    }
}
