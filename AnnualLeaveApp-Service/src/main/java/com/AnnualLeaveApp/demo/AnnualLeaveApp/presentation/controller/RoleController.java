package com.AnnualLeaveApp.demo.AnnualLeaveApp.presentation.controller;

import com.AnnualLeaveApp.demo.AnnualLeaveApp.data.dto.swagger.RoleDTO;
import com.AnnualLeaveApp.demo.AnnualLeaveApp.data.repo.LeaveTypeRepo;
import com.AnnualLeaveApp.demo.AnnualLeaveApp.domain.services.provider.RoleProvider;
import com.AnnualLeaveApp.demo.AnnualLeaveApp.domain.util.ObjectMapper;
import com.AnnualLeaveApp.demo.AnnualLeaveApp.presentation.swagger.RoleApi;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@Controller
@RequestMapping("/api/v1")
public class RoleController implements RoleApi {

    @Autowired
    RoleProvider roleProvider;

    @Autowired
    HttpServletRequest httpServletRequest;

    @Override
    public Optional<HttpServletRequest> getRequest() {
        return Optional.of(httpServletRequest);
    }

    @Override
    public ResponseEntity<List<RoleDTO>> findAllRoles() {
        return ResponseEntity.ok(
                roleProvider
                        .findAll()
                        .stream()
                        .map(ObjectMapper::map)
                        .collect(Collectors.toList()));
    }
}
