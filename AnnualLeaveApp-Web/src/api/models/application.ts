/* tslint:disable */
export interface Application {
  id?: string;
  firstName?: string;
  lastName?: string;
  startDate?: string;
  endDate?: string;
  leaveTypeId?: number;
  applicationStatus?: string;
  description?: string;
}
