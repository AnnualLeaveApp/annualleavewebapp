package com.AnnualLeaveApp.demo.AnnualLeaveApp.data.repo;

import com.AnnualLeaveApp.demo.AnnualLeaveApp.data.model.ApplicationStatus;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;
import java.util.UUID;

public interface ApplicationStatusRepo extends JpaRepository<ApplicationStatus, Integer> {
    Optional<ApplicationStatus> findByStatus(String status);
}
