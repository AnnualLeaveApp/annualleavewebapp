package com.AnnualLeaveApp.demo.AnnualLeaveApp.domain.services.provider;


import com.AnnualLeaveApp.demo.AnnualLeaveApp.data.model.MailTemplate;
import com.AnnualLeaveApp.demo.AnnualLeaveApp.data.repo.MailTemplateRepo;
import com.AnnualLeaveApp.demo.AnnualLeaveApp.domain.provider.CrudProvider;

import java.util.UUID;

public interface MailTemplateProvider extends CrudProvider<MailTemplate, UUID, MailTemplateRepo> {
  MailTemplate findByLangAndAndSubject (String lang, String subject);
}
