import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import { RoleGuardService as RoleGuard } from '../../guards';

const routes: Routes = [
  {
    path: '',
    data: {
      title: 'Administration',
      status: false
    },
    children: [
      {
        path: 'users',
        loadChildren: './user/userlist.module#UserlistModule',
        canActivate: [RoleGuard], 
        data: 
        { 
          expectedRole: 'ADMIN'
        },
      },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule { }
