package com.AnnualLeaveApp.demo.AnnualLeaveApp.domain.services;

import com.AnnualLeaveApp.demo.AnnualLeaveApp.data.model.MailTemplate;
import com.AnnualLeaveApp.demo.AnnualLeaveApp.data.repo.MailTemplateRepo;
import com.AnnualLeaveApp.demo.AnnualLeaveApp.domain.provider.AbstractCrudProvider;
import com.AnnualLeaveApp.demo.AnnualLeaveApp.domain.services.provider.MailTemplateProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.UUID;

@Service
public class MailTemplateService extends AbstractCrudProvider<MailTemplate, UUID, MailTemplateRepo> implements MailTemplateProvider {

  @Autowired
  MailTemplateRepo mailTemplateRepo;

  @Override
  public MailTemplate findByLangAndAndSubject(String lang, String subject) {
    Optional<MailTemplate> mailTemplateOptional = mailTemplateRepo.findByLangAndAndSubject(lang, subject);
    if (!mailTemplateOptional.isPresent()) {
      // TODO throw a mail lang exception
    }

    return mailTemplateOptional.get();
  }

  @Override
  protected MailTemplateRepo getRepo() {
    return mailTemplateRepo;
  }

  @Override
  protected String onUpdateObjectNotFoundMessage() {
    return "There is no MailTemplate with this id in our database please try again";
  }

}
