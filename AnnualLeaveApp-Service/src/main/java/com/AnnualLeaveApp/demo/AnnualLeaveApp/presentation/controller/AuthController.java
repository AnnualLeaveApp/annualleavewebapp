package com.AnnualLeaveApp.demo.AnnualLeaveApp.presentation.controller;

import com.AnnualLeaveApp.demo.AnnualLeaveApp.data.dto.swagger.LoginDTO;
import com.AnnualLeaveApp.demo.AnnualLeaveApp.data.dto.swagger.UserDTO;
import com.AnnualLeaveApp.demo.AnnualLeaveApp.data.model.User;
import com.AnnualLeaveApp.demo.AnnualLeaveApp.domain.services.provider.UserProvider;
import com.AnnualLeaveApp.demo.AnnualLeaveApp.domain.util.ObjectMapper;
import com.AnnualLeaveApp.demo.AnnualLeaveApp.presentation.swagger.AuthApi;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.Optional;

@RestController
@Controller
@RequestMapping("/api/v1")
public class AuthController implements AuthApi {

    @Autowired
    UserProvider userProvider;

    @Autowired
    HttpServletRequest httpServletRequest;

    @Override
    public Optional<HttpServletRequest> getRequest() {
        return Optional.of(httpServletRequest);
    }

    @Override
    public ResponseEntity<UserDTO> login(@Valid LoginDTO login) {
        UserDTO userDTO = ObjectMapper.map(userProvider.login(ObjectMapper.map(login)));
        userDTO.setRemainingLeaveDays(userProvider.getRemainingAnnualLeaveDays(ObjectMapper.map(userDTO.getPerson())));
        return ResponseEntity.ok(userDTO);
    }

    @Override
    public ResponseEntity<UserDTO> findLoggegUser() {
        UserDTO userDTO = ObjectMapper.map(ObjectMapper.map(getRequest()).get());
        userDTO.setRemainingLeaveDays(userProvider.getRemainingAnnualLeaveDays(ObjectMapper.map(userDTO.getPerson())));
        return ResponseEntity.of(Optional.of(userDTO));
    }
}
