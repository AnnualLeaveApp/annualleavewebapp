package com.AnnualLeaveApp.demo.AnnualLeaveApp.data.repo;

import com.AnnualLeaveApp.demo.AnnualLeaveApp.data.model.Person;
import com.AnnualLeaveApp.demo.AnnualLeaveApp.data.model.Role;
import com.AnnualLeaveApp.demo.AnnualLeaveApp.data.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;
import java.util.UUID;

public interface UserRepo extends JpaRepository<User, UUID> {

    Optional<User> findFirstByUsername(String username);

    Optional<User> findFirstByPerson(Person person);

    Optional<User> findFirstByRoleList(Role role);

}
