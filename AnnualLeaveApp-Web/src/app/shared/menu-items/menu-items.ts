import { Injectable } from '@angular/core';


export class BadgeItem {
  type: string;
  value: string;
}

export class ChildrenItems {
  state: string;
  target?: boolean;
  name: string;
  type?: string;
  children?: ChildrenItems[];
}

export class MainMenuItems {
  state: string;
  short_label?: string;
  main_state?: string;
  target?: boolean;
  name: string;
  type: string;
  icon: string;
  badge?: BadgeItem[];
  children?: ChildrenItems[];
}

export class Menu {
  label: string;
  main: MainMenuItems[];
}




@Injectable()
export class MenuItems {

  constructor() {
  }
}
