package com.AnnualLeaveApp.demo.AnnualLeaveApp.data.model;

public class UserToken extends User {
    private String token;

    public UserToken(User user) {
        setPassword(user.getPassword());
        setUsername(user.getUsername());
        setEmail(user.getEmail());
        setId(user.getId());
        setVerificationToken(user.getVerificationToken());
        setDeleted(user.isDeleted());
        setDateModified(user.getDateModified());
        setDateCreated(user.getDateCreated());
        setRoleList(user.getRoleList());
        setPerson(user.getPerson());
    }

    public UserToken() {
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
