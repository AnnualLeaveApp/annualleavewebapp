import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';

import { NavigationService } from '../services/navigation.service';
import swal from 'sweetalert2';

@Injectable()
export class ErrorInterceptor implements HttpInterceptor {
    constructor(private navigationService: NavigationService) { }

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        return next.handle(request).pipe(catchError(err => {
            if (err.status === 403) {
                var token = localStorage.getItem('userJWT');
                if (token != null) {
                    // auto logout if 401 response returned from api
                    this.navigationService.logOutUser();
                    location.reload(true);
                }
            }

            else if (err.status === 400) {
                swal('Wrong Input!',
                    'Input data are not valid!',
                    'warning'
                );
            }

            
            else if (err.status === 404) {
                swal('No data!',
                `${err.error.message}`,
                    'warning'
                );
            }

            else if (err.status === 409) {
                swal('Intersecment Error!',
                `${err.error.message}`,
                    'warning'
                );
            }

            else if (err.status === 502) {
            }

            return throwError(err);
        }))
    }
}