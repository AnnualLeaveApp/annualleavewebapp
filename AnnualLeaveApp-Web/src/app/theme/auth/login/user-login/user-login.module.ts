import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UserLoginComponent } from './user-login.component';
import {UserLoginRoutingModule} from './user-login-routing.module';
import {SharedModule} from '../../../../shared/shared.module';
import { FormService } from '../../../../services/validators/services';

@NgModule({
  imports: [
    CommonModule,
    UserLoginRoutingModule,
    SharedModule
  ],
  providers:  [FormService],
  declarations: [UserLoginComponent]
})
export class UserLoginModule { }
