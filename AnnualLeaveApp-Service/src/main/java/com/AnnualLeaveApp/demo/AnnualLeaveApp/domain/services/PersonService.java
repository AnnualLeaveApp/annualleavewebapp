package com.AnnualLeaveApp.demo.AnnualLeaveApp.domain.services;

import com.AnnualLeaveApp.demo.AnnualLeaveApp.data.model.Person;
import com.AnnualLeaveApp.demo.AnnualLeaveApp.data.repo.PersonRepo;
import com.AnnualLeaveApp.demo.AnnualLeaveApp.domain.provider.AbstractCrudProvider;
import com.AnnualLeaveApp.demo.AnnualLeaveApp.domain.services.provider.PersonProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
public class PersonService extends AbstractCrudProvider<Person, UUID, PersonRepo> implements PersonProvider {

    @Autowired
    PersonRepo personRepo;

    @Override
    protected PersonRepo getRepo() {
        return personRepo;
    }
    @Override
    protected String onUpdateObjectNotFoundMessage() {
        return "There is no Person with this id in our database please try again";
    }
}
