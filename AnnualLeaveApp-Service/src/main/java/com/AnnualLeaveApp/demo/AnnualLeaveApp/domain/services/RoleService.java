package com.AnnualLeaveApp.demo.AnnualLeaveApp.domain.services;

import com.AnnualLeaveApp.demo.AnnualLeaveApp.data.model.Role;
import com.AnnualLeaveApp.demo.AnnualLeaveApp.data.repo.RoleRepo;
import com.AnnualLeaveApp.demo.AnnualLeaveApp.domain.provider.AbstractCrudProvider;
import com.AnnualLeaveApp.demo.AnnualLeaveApp.domain.services.provider.RoleProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.UUID;

@Service
public class RoleService extends AbstractCrudProvider<Role, UUID, RoleRepo> implements RoleProvider {

    @Autowired
    RoleRepo roleRepo;

    @Override
    protected RoleRepo getRepo() {
        return roleRepo;
    }

    @Override
    public Role findRoleByName(String name) {
        Optional<Role> optionalRole = roleRepo.findByName(name);
        if (!optionalRole.isPresent()) {
            // TODO throw a role name exception
        }

        return optionalRole.get();
    }
    @Override
    protected String onUpdateObjectNotFoundMessage() {
        return "There is no Role with this id in our database please try again";
    }

}
