import { ChangeDetectionStrategy, ChangeDetectorRef, Component, OnInit, ViewChild, ViewEncapsulation, Output, EventEmitter, ElementRef } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { animate, style, transition, trigger } from '@angular/animations';
import swal from 'sweetalert2';
import { FormBuilder, FormGroup, Validators, FormArray, FormControl, ValidatorFn } from '@angular/forms';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/filter';
import { FormService } from 'src/app/services/validators/services';
import { ApplicationService, LeaveTypeService, UserService, AuthService } from '../../../../api/services';
import { MatPaginator, MatSort } from '@angular/material';
import { User, Application, LeaveType } from '../../../../api/models';
import { CustomValidators } from 'src/app/services/validators/modal.custom.validators';
import { IMyDpOptions } from 'mydatepicker';
import { Helper } from 'src/app/helpers/helper';
import { fromEvent } from 'rxjs';
import { ApplicationsDataSource } from 'src/app/services/datatables/home-applications.datatable.service';



@Component({
  selector: 'app-user-applications',
  templateUrl: './user-home.component.html',
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush,
  styleUrls: [
    './user-home.component.scss',
    '../../../../assets/icon/icofont/css/icofont.scss'
  ],
  animations: [
    trigger('fadeInOutTranslate', [
      transition(':enter', [
        style({ opacity: 0 }),
        animate('400ms ease-in-out', style({ opacity: 1 }))
      ]),
      transition(':leave', [
        style({ transform: 'translate(0)' }),
        animate('400ms ease-in-out', style({ opacity: 0 }))
      ])
    ])
  ]
})
export class UserHomeComponent implements OnInit {
  /** >> MATERIAL DATA TABLE */
  displayedColumns = ["startDate", "endDate", "leaveType", "status", "actions"];
  dataSource: any | null;
  index: number;
  id: number;

  @ViewChild(MatPaginator, null) paginator: MatPaginator;
  @ViewChild(MatSort, null) sort: MatSort;


  /** << MATERIAL DATA TABLE */

  // general entity types memebers
  public applicationList: Application[];
  public leaveTypes: LeaveType[] = [];
  public currentUser: User;

  // selected form entity value
  public selectedApplication: Application;
  public selectedLeaveType: LeaveType;

  private initDate = new Date();
  public date: any = { date: { year: (this.initDate).getFullYear(), month: (this.initDate).getMonth() + 1, day: (this.initDate).getDate() } }
  public startDate: any = { date: { year: (this.initDate).getFullYear(), month: (this.initDate).getMonth() + 1, day: (this.initDate).getDate() } }
  public endDate: any = { date: { year: (this.initDate).getFullYear(), month: (this.initDate).getMonth() + 1, day: (this.initDate).getDate() } }
  public startDatePickerModel: any = { date: { year: this.initDate.getFullYear(), month: this.initDate.getMonth(), day: this.initDate.getDate() } };
  public endDatePickerModel: any = { date: { year: this.initDate.getFullYear(), month: this.initDate.getMonth(), day: this.initDate.getDate() } };

  //modal & form variables
  public startDatePickerOptions: IMyDpOptions =
    {
      dateFormat: 'dd/mm/yyyy',
      minYear: this.initDate.getFullYear() - 1,
      maxYear: this.initDate.getFullYear() + 1,
      markCurrentDay: true,
    };
  public endDatePickerOptions: IMyDpOptions =
    {
      dateFormat: 'dd/mm/yyyy',
      minYear: this.initDate.getFullYear(),
      maxYear: this.initDate.getFullYear() + 1,
      markCurrentDay: true
    };

  //modal & form variables
  public saveBtn = "Save";
  applicationModalForm: FormGroup;
  public applicationModalFormErrors = {
    application_startDateInput: '',
    application_endDateInput: '',
    leaveTypeDPList: ''
  };
  loadingModal = false;
  submitted = false;
  public isFormReady = false;
  isCreateOperation = false;
  isUpdateOperation = false;
  actionBasedTitle = "";

  constructor(private fb: FormBuilder, private chRef: ChangeDetectorRef, public FormService: FormService,
    public http: HttpClient, private applicationService: ApplicationService, private authService: AuthService,
    private leaveTypeService: LeaveTypeService,
    private helper: Helper) { }

  ngOnInit() {
    this.initializeApplicationData();
    this.loadData();
  }

  //#region Initialization Operations

  initializeApplicationData() {
    this.applicationService.findAllByUser().subscribe(
      response => {
        this.applicationList = response;
      },
      err => {
        swal(
          'System Error',
          'Please provide this error to the IT Department!',
          'error'
        );
      }
    );

    this.authService.findLoggegUser().subscribe(
      response => {
        this.currentUser = response as User;
      },
      err => {
        swal(
          'System Error',
          'Please provide this error to the IT Department!',
          'error'
        );
      }
    );

    this.leaveTypeService.findAllTypes().subscribe(
      response => {
        this.leaveTypes = response;
      },
      err => {
        swal(
          'System Error',
          'Please provide this error to the IT Department!',
          'error'
        );
      }
    );

    this.initializeApplicationModal()
  }

  initializeApplicationModal() {

    this.applicationModalForm = this.fb.group({
      application_startDateInput:
        [
          '',
          [Validators.required, CustomValidators.ValidApplicationDateRanges]
        ],
      application_endDateInput:
        [
          '',
          [Validators.required, CustomValidators.ValidApplicationDateRanges]
        ],
      leaveTypeDPList:
        [
          '',
          Validators.required
        ],
    });

    this.applicationModalForm.valueChanges.subscribe((data) => {
      this.applicationModalFormErrors = this.FormService.validateForm(this.applicationModalForm, this.applicationModalFormErrors, true)
    });

    this.isFormReady = true;
  }

  //#endregion

  //#region CRUD Operations over user

  makeCreationCall(modalFormValues: any) {
    let postBody: Application = {
      startDate: this.helper.formatDateString(modalFormValues.application_startDateInput.date),
      endDate: this.helper.formatDateString(modalFormValues.application_endDateInput.date),
      leaveTypeId: this.selectedLeaveType.id
    }

    this.applicationService.create(postBody).subscribe(
      (data: any) => {
        if (data) {
          this.loadingModal = false;
          this.refresh();
          swal(
            "Application created!",
            "The record has been successfully saved!",
            'success'
          );
          this.closeMyModal('ApplicationModal');
        }
      },
      error => {
        this.loadingModal = false;
        this.closeMyModal('ApplicationModal');
      });
  }

  makeUpdateCall(modalFormValues: any) {
    let appRequest: Application = {
      startDate: this.helper.formatDateString(modalFormValues.application_startDateInput.date),
      endDate: this.helper.formatDateString(modalFormValues.application_endDateInput.date),
      leaveTypeId: this.selectedLeaveType.id
    }
    let params: ApplicationService.UpdateParams = {
      applicationId: this.selectedApplication.id,
      application: appRequest
    }

    this.applicationService.update(params).subscribe(
      (data: any) => {
        if (data) {
          this.loadingModal = false;
          this.refresh();
          swal(
            "Application updated!",
            "The record has been successfully updated!",
            'success'
          );
          this.closeMyModal('ApplicationModal');
        }
      },
      error => {
        this.loadingModal = false;
        this.closeMyModal('ApplicationModal');
      });
  }

  makeDeleteCall(selectedAppId: string) {
    this.applicationService.delete(selectedAppId).subscribe(
      (data: any) => {
        if (data) {
          this.loadingModal = false;
          this.refresh();
          swal(
            "App deleted!",
            "The record has been successfully deleted!",
            'success'
          );
          this.closeMyModal('ApplicationModal');
        }
      },
      error => {
        this.loadingModal = false;
        this.closeMyModal('ApplicationModal');
      });
  }

  //#endregion

  //#region UI, Modal & Form Methods

  saveApplication() {
    this.submitted = true;

    if (this.applicationModalForm.invalid) {
      this.applicationModalForm = this.FormService.validateForm(this.applicationModalForm, this.applicationModalFormErrors, false)
      return;
    }
    this.submitted = false;

    const val = this.applicationModalForm.value;

    if (this.isCreateOperation) {
      this.makeCreationCall(val);
      this.isCreateOperation = !this.isCreateOperation;
    }

    if (this.isUpdateOperation) {
      this.makeUpdateCall(val);
      this.isUpdateOperation = !this.isUpdateOperation;
    }
  }

  openMyModal(modalID) {
    document.querySelector('#' + modalID).classList.add('md-show');
  }

  closeMyModal(modalID) {
    this.submitted = false;
    this.loadingModal = false;
    this.isUpdateOperation = false;
    this.isCreateOperation = false;
    this.saveBtn = "Save";
    if (modalID == "ApplicationModal") {
      document.querySelector('#' + modalID).classList.remove('md-show');
    }
    else {
      ((modalID.target.parentElement.parentElement).parentElement).classList.remove('md-show');
    }
    this.applicationModalForm.reset();
  }


  setUserFormValuesOnEventUpdate(): void {

    let startDateApp = this.getFormatedDate(this.selectedApplication.startDate);
    let endDateApp = this.getFormatedDate(this.selectedApplication.endDate);

    this.applicationModalForm.patchValue({
      application_startDateInput: {
        date: {
          year: startDateApp.getFullYear(),
          month: startDateApp.getMonth() + 1,
          day: startDateApp.getDate()
        }
      },
      application_endDateInput:{
        date: {
          year: endDateApp.getFullYear(),
          month: endDateApp.getMonth() + 1,
          day: endDateApp.getDate()
        }
      },
      leaveTypeDPList: this.leaveTypes.filter(leaveType => leaveType.id == this.selectedLeaveType.id)[0]
    }, { onlySelf: true, emitEvent: true });

    this.startDate = this.applicationModalForm.controls.application_startDateInput.value;
    this.endDate = this.applicationModalForm.controls.application_endDateInput.value;
  }

  //#endregion

  //#region Private Helper Methods

  OnChangeLeaveTypeSite(selectedType: LeaveType) {
    if (selectedType) {
      this.selectedLeaveType = selectedType;
    }
  }

  public loadData() {

    this.dataSource = new ApplicationsDataSource(
      this.paginator,
      this.sort,
      null,
      this.applicationService
    );
  }

  //** >> MATERIAL DATA TABLE */
  refresh() {
    this.loadData();
  }

  handleEvent(action: string, appToUpdate?: Application): void {
    switch (action) {
      case "Create":
        this.actionBasedTitle = "New Annual Leave";
        this.isCreateOperation = true;
        this.isUpdateOperation = false;
        this.openMyModal('ApplicationModal');
        break;
      case "Update":
        this.actionBasedTitle = "Update Annual Leave";
        this.isUpdateOperation = true;
        this.isCreateOperation = false;
        this.isUpdateOperation = true;
        this.selectedApplication = appToUpdate;
        this.selectedLeaveType = this.leaveTypes.filter(leaveType => leaveType.id == appToUpdate.leaveTypeId)[0];
        this.setUserFormValuesOnEventUpdate();
        this.openMyModal('ApplicationModal')
        break;
    }
  }

  deleteSelectedApplication(appToDelete: Application) {
    swal({
      title: 'Attention!',
      text: "Are you sure to delete application?",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete!',
      cancelButtonText: 'No!',
    }).then((makeDelete) => {
      if (makeDelete.value) {
        this.makeDeleteCall(appToDelete.id);
      }
    });
  }

  getLeaveTypeName(leaveTypeId :number){
    return this.leaveTypes.filter(leaveType => leaveType.id == leaveTypeId)[0].type
  }

  getFormatedDate(date) {
    return this.helper.formatDateTimeFromString(date);
  }

  //#endregion

}
