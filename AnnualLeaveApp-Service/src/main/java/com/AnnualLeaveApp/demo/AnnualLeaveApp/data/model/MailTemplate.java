package com.AnnualLeaveApp.demo.AnnualLeaveApp.data.model;

import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Objects;

@Entity
@Table(name = "mail_template", schema = "lhind_demo")
@SQLDelete(sql = "UPDATE mail_template SET is_deleted = 1 WHERE id = ?")
@Where(clause = " is_deleted = 0")
public class MailTemplate extends BaseSoftDeleteEntity{
    private String subject;
    private String message;
    private String lang;

    @Basic
    @Column(name = "subject")
    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    @Basic
    @Column(name = "message")
    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Basic
    @Column(name = "lang")
    public String getLang() {
        return lang;
    }

    public void setLang(String lang) {
        this.lang = lang;
    }
}
