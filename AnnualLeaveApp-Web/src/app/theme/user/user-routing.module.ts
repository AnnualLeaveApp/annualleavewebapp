import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import { RoleGuardService as RoleGuard } from '../../guards';

const routes: Routes = [
  {
    path: '',
    data: {
      title: 'User',
      status: false
    },
    children: [
      {
        path: 'home',
        loadChildren: './home/user-home.module#UserHomeModule',
        canActivate: [RoleGuard], 
        data: 
        { 
          expectedRole: 'USER'
        },
      },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UserRoutingModule { }
