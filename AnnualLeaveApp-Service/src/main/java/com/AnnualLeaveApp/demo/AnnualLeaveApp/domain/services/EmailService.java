package com.AnnualLeaveApp.demo.AnnualLeaveApp.domain.services;

import com.AnnualLeaveApp.demo.AnnualLeaveApp.data.model.MailTemplate;
import com.AnnualLeaveApp.demo.AnnualLeaveApp.data.model.SupervisorMail;
import com.AnnualLeaveApp.demo.AnnualLeaveApp.data.model.UserMail;
import com.AnnualLeaveApp.demo.AnnualLeaveApp.domain.services.provider.EmailProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

@Service
public class EmailService implements EmailProvider {

  @Autowired
  private JavaMailSender javaMailSender;

  @Async
  @Override
  public void sendMail(SimpleMailMessage simpleMailMessage) {
    this.javaMailSender.send(simpleMailMessage);
  }


  @Override
  public SimpleMailMessage createUserNotificationMail(String mailTo, String mailFrom, MailTemplate mail, UserMail userMailData) {
    SimpleMailMessage mailMessage = setMailMessageData(mailTo, mailFrom, mail);
    mailMessage.setText(createUserNotificationMailBody(mail, userMailData));
    return mailMessage;
  }

  @Override
  public String createUserNotificationMailBody(MailTemplate mail, UserMail userMailData) {
    return mail.getMessage()
            .replace("first_name", userMailData.getName())
            .replace("leave_type", userMailData.getLeaveType())
            .replace("application_status", userMailData.getApplicationStatus())
            .replace("start_date", userMailData.getStartDate().toString())
            .replace("end_date", userMailData.getEndDate().toString());
  }

  @Override
  public SimpleMailMessage createSupervisorNotificationMail(String mailTo, String mailFrom, MailTemplate mail, SupervisorMail supervisorMailData) {
    SimpleMailMessage mailMessage = setMailMessageData(mailTo, mailFrom, mail);
    mailMessage.setText(createSupervisorNotificationMailBody(mail, supervisorMailData));
    return mailMessage;
  }

  @Override
  public String createSupervisorNotificationMailBody(MailTemplate mail, SupervisorMail supervisorMailData) {
    return mail.getMessage()
            .replace("user_name", supervisorMailData.getUser_name())
            .replace("supervisor_name", supervisorMailData.getName())
            .replace("leave_type", supervisorMailData.getLeaveType())
            .replace("start_date", supervisorMailData.getStartDate().toString())
            .replace("end_date", supervisorMailData.getEndDate().toString());
  }

  private SimpleMailMessage setMailMessageData(String mailTo, String mailFrom, MailTemplate mail){
    SimpleMailMessage mailMessage = new SimpleMailMessage();
    mailMessage.setTo(mailTo);
    mailMessage.setFrom(mailFrom);
    mailMessage.setSubject(mail.getSubject());
    return mailMessage;
  }


}
