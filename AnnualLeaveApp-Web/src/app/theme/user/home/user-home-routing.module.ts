import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import { UserHomeComponent } from './user-home.component';

const routes: Routes = [
  {
    path: '',
    component: UserHomeComponent,
    data: {
      title: 'Applications',
      icon: 'icon-widgetized',
      status: true
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UserHomeRoutingModule { }
