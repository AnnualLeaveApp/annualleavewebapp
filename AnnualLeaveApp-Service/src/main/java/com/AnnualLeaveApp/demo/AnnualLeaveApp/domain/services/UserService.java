package com.AnnualLeaveApp.demo.AnnualLeaveApp.domain.services;

import com.AnnualLeaveApp.demo.AnnualLeaveApp.data.ExceptionType;
import com.AnnualLeaveApp.demo.AnnualLeaveApp.data.GeneralException;
import com.AnnualLeaveApp.demo.AnnualLeaveApp.data.model.*;
import com.AnnualLeaveApp.demo.AnnualLeaveApp.data.repo.UserRepo;
import com.AnnualLeaveApp.demo.AnnualLeaveApp.domain.config.security.PasswordProvider;
import com.AnnualLeaveApp.demo.AnnualLeaveApp.domain.config.security.TokenProvider;
import com.AnnualLeaveApp.demo.AnnualLeaveApp.domain.provider.AbstractCrudProvider;
import com.AnnualLeaveApp.demo.AnnualLeaveApp.domain.services.provider.*;
import com.AnnualLeaveApp.demo.AnnualLeaveApp.domain.util.Helper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import javax.management.relation.RoleList;
import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class UserService extends AbstractCrudProvider<User, UUID, UserRepo> implements UserProvider {

    @Autowired
    PasswordProvider passwordProvider;
    @Autowired
    TokenProvider tokenProvider;
    @Autowired
    UserRepo userRepo;
    @Autowired
    UserProvider userProvider;
    @Autowired
    RoleProvider roleProvider;
    @Autowired
    PersonProvider personProvider;
    @Autowired
    ContractProvider contractProvider;
    @Autowired
    ApplicationProvider applicationProvider;
    @Autowired
    MailTemplateProvider mailTemplateProvider;
    @Autowired
    EmailProvider emailProvider;

    @Override
    protected UserRepo getRepo() {
        return userRepo;
    }

    @Override
    public UserToken login(User loginUser) {
        Optional<User> user = userRepo.findFirstByUsername(loginUser.getUsername());
        if (!(user.isPresent()))
            throw new UsernameNotFoundException("User or Password wrong!");
        if (!passwordProvider.matchPassword(loginUser.getPassword(), user.get().getPassword()))
            throw new UsernameNotFoundException("User or Password wrong!");
        return createUserToken(user.get());
    }

    @Override
    public User findLoggedInUser(User user) {
        return userRepo.findFirstByUsername(user.getUsername()).get();
    }

    public String deleteUser(User user) {
        userRepo.delete(findLoggedInUser(user));
        return user.getUsername();
    }

    @Override
    public int getRemainingAnnualLeaveDays(Person person) {
        User user = userRepo.findFirstByPerson(person).get();
        Optional<Contract> contract = contractProvider.getUserContractInfo(user.getId());
        int annualLeaveDays = contract.map(Contract::getAnnualLeaveDays).orElse(0);
        int totalAppliedLeaveDays = applicationProvider.getAllPersonAnnualLeaveApplication(person)
                .stream().mapToInt(app -> Helper.getTotalDaysBetweenDates(app.getStartDate(), app.getEndDate()))
                .sum();
        return annualLeaveDays - totalAppliedLeaveDays;
    }

    public Optional<User> findByUsername(String username) {
        return userRepo.findFirstByUsername(username);
    }

    @Transactional
    public User create(User user, Contract contract, User requestUser) {
        generateDefaultCredentials(user, false);
        user.setRoleList(getRoleList(user.getRoleList().stream().map(Role::getId).collect(Collectors.toList())));
        Person createdPerson = personProvider.create(user.getPerson(), Optional.of(requestUser));
        user.setPerson(createdPerson);
        contract.setPerson(createdPerson);
        contractProvider.create(contract, Optional.of(requestUser));
        return userProvider.create(user, Optional.of(requestUser));
    }

    @Transactional
    public User update(UUID id, User user, Contract contract, User requestUser) {
        Optional<User> existingUser = userProvider.findById(id);
        if (existingUser.isPresent()) {
            user.setId(existingUser.get().getId());
            generateDefaultCredentials(user, true);
            user.setRoleList(getRoleList(user.getRoleList().stream().map(Role::getId).collect(Collectors.toList())));
            Person updatedPerson = personProvider.update(user.getPerson(), existingUser.get().getPerson().getId(),Optional.of(requestUser)).get();
            user.setPerson(updatedPerson);
            contract.setPerson(updatedPerson);
            User updatedUser = userProvider.update(user, id, Optional.of(requestUser)).get();
            Optional<Contract> existingContract = contractProvider.getUserContractInfo(user.getId());
            if (existingContract.isPresent()) {
                contractProvider.update(contract, existingContract.get().getId(), Optional.of(requestUser));
            }
            return updatedUser;
        }
        throw new GeneralException(ExceptionType.CONFLICT, "User", "User not found!", id);
    }

    private UserToken createUserToken(User user) {
        UserToken userToken = new UserToken(user);
        userToken.setToken(tokenProvider.createToken(user));
        return userToken;
    }

    private List<Role> getRoleList(String roleName) {
        List<Role> roles = new ArrayList<>();
        Role unverifiedRole = roleProvider.findRoleByName(roleName);
        roles.add(unverifiedRole);
        return roles;
    }

    private List<Role> getRoleList(List<UUID> roleIds) {
        List<Role> roles = new ArrayList<>();
        roleIds.forEach(id -> roles.add(roleProvider.findById(id).get()));
        return roles;
    }

    private User generateDefaultCredentials(User user, boolean isUpdate) {
        Optional<User> existingUser;
        existingUser = userRepo.findFirstByUsername(user.getUsername());
        if ((!isUpdate && existingUser.isPresent()) || (isUpdate && !existingUser.isPresent()) ||
                ((!existingUser.isPresent() || existingUser.get().getId() != user.getId()) && !isUpdate)) {
            user.setPassword(passwordProvider.encodePassword(user.getUsername().toLowerCase()));
            return user;
        }
        throw new GeneralException(ExceptionType.CONFLICT, "User", "Username already exists!", user.getUsername());
    }

    @Override
    protected String onUpdateObjectNotFoundMessage() {
        return "There is no User with this id in our database please try again";
    }
}
