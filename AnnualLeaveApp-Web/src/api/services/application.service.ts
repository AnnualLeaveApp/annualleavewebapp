/* tslint:disable */
import { Injectable } from '@angular/core';
import { HttpClient, HttpRequest, HttpResponse, HttpHeaders } from '@angular/common/http';
import { BaseService as __BaseService } from '../base-service';
import { ApiConfiguration as __Configuration } from '../api-configuration';
import { StrictHttpResponse as __StrictHttpResponse } from '../strict-http-response';
import { Observable as __Observable } from 'rxjs';
import { map as __map, filter as __filter } from 'rxjs/operators';

import { Application } from '../models/application';
import { RejectRequest } from '../models/reject-request';
@Injectable({
  providedIn: 'root',
})
class ApplicationService extends __BaseService {
  static readonly createPath = '/application';
  static readonly findAllPath = '/application';
  static readonly findAllByUserPath = '/application/user';
  static readonly updatePath = '/application/{applicationId}';
  static readonly findByIdPath = '/application/{applicationId}';
  static readonly deletePath = '/application/{applicationId}';
  static readonly rejectPath = '/application/{applicationId}/status/reject';
  static readonly approvePath = '/application/{applicationId}/status/approve';

  constructor(
    config: __Configuration,
    http: HttpClient
  ) {
    super(config, http);
  }

  /**
   * @param application undefined
   * @return OK
   */
  createResponse(application: Application): __Observable<__StrictHttpResponse<Application>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    __body = application;
    let req = new HttpRequest<any>(
      'POST',
      this.rootUrl + `/application`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<Application>;
      })
    );
  }
  /**
   * @param application undefined
   * @return OK
   */
  create(application: Application): __Observable<Application> {
    return this.createResponse(application).pipe(
      __map(_r => _r.body as Application)
    );
  }

  /**
   * @return OK
   */
  findAllResponse(): __Observable<__StrictHttpResponse<Array<Application>>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/application`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<Array<Application>>;
      })
    );
  }
  /**
   * @return OK
   */
  findAll(): __Observable<Array<Application>> {
    return this.findAllResponse().pipe(
      __map(_r => _r.body as Array<Application>)
    );
  }

  /**
   * @return OK
   */
  findAllByUserResponse(): __Observable<__StrictHttpResponse<Array<Application>>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/application/user`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<Array<Application>>;
      })
    );
  }
  /**
   * @return OK
   */
  findAllByUser(): __Observable<Array<Application>> {
    return this.findAllByUserResponse().pipe(
      __map(_r => _r.body as Array<Application>)
    );
  }

  /**
   * @param params The `ApplicationService.UpdateParams` containing the following parameters:
   *
   * - `applicationId`:
   *
   * - `application`:
   *
   * @return OK
   */
  updateResponse(params: ApplicationService.UpdateParams): __Observable<__StrictHttpResponse<Application>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    __body = params.application;
    let req = new HttpRequest<any>(
      'PUT',
      this.rootUrl + `/application/${params.applicationId}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<Application>;
      })
    );
  }
  /**
   * @param params The `ApplicationService.UpdateParams` containing the following parameters:
   *
   * - `applicationId`:
   *
   * - `application`:
   *
   * @return OK
   */
  update(params: ApplicationService.UpdateParams): __Observable<Application> {
    return this.updateResponse(params).pipe(
      __map(_r => _r.body as Application)
    );
  }

  /**
   * @param applicationId undefined
   * @return OK
   */
  findByIdResponse(applicationId: string): __Observable<__StrictHttpResponse<Application>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/application/${applicationId}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<Application>;
      })
    );
  }
  /**
   * @param applicationId undefined
   * @return OK
   */
  findById(applicationId: string): __Observable<Application> {
    return this.findByIdResponse(applicationId).pipe(
      __map(_r => _r.body as Application)
    );
  }

  /**
   * @param applicationId undefined
   * @return OK
   */
  deleteResponse(applicationId: string): __Observable<__StrictHttpResponse<string>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    let req = new HttpRequest<any>(
      'DELETE',
      this.rootUrl + `/application/${applicationId}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'text'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<string>;
      })
    );
  }
  /**
   * @param applicationId undefined
   * @return OK
   */
  delete(applicationId: string): __Observable<string> {
    return this.deleteResponse(applicationId).pipe(
      __map(_r => _r.body as string)
    );
  }

  /**
   * @param params The `ApplicationService.RejectParams` containing the following parameters:
   *
   * - `rejectRequest`:
   *
   * - `applicationId`:
   *
   * @return OK
   */
  rejectResponse(params: ApplicationService.RejectParams): __Observable<__StrictHttpResponse<Application>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    __body = params.rejectRequest;

    let req = new HttpRequest<any>(
      'PUT',
      this.rootUrl + `/application/${params.applicationId}/status/reject`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<Application>;
      })
    );
  }
  /**
   * @param params The `ApplicationService.RejectParams` containing the following parameters:
   *
   * - `rejectRequest`:
   *
   * - `applicationId`:
   *
   * @return OK
   */
  reject(params: ApplicationService.RejectParams): __Observable<Application> {
    return this.rejectResponse(params).pipe(
      __map(_r => _r.body as Application)
    );
  }

  /**
   * @param applicationId undefined
   * @return OK
   */
  approveResponse(applicationId: string): __Observable<__StrictHttpResponse<Application>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    let req = new HttpRequest<any>(
      'PUT',
      this.rootUrl + `/application/${applicationId}/status/approve`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<Application>;
      })
    );
  }
  /**
   * @param applicationId undefined
   * @return OK
   */
  approve(applicationId: string): __Observable<Application> {
    return this.approveResponse(applicationId).pipe(
      __map(_r => _r.body as Application)
    );
  }
}

module ApplicationService {

  /**
   * Parameters for update
   */
  export interface UpdateParams {
    applicationId: string;
    application: Application;
  }

  /**
   * Parameters for reject
   */
  export interface RejectParams {
    rejectRequest: RejectRequest;
    applicationId: string;
  }
}

export { ApplicationService }
