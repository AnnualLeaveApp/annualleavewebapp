import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Type } from '@angular/compiler/src/output/output_ast';

@Injectable()
export class JwtInterceptor implements HttpInterceptor {
    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        // add authorization header with jwt token if available
        let token = localStorage.getItem('userJWT');
        if (token) {
            request = request.clone({
                headers: new HttpHeaders({ 
                    'Authorization': `Bearer ${token}`,
                    'Content-Type': 'application/json'
                })
            });
        }
        return next.handle(request);
    }
}