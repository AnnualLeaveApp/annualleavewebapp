package com.AnnualLeaveApp.demo.AnnualLeaveApp.domain.config.security;

public class SecurityConstants {

  public static final String SIGN_UP_URLS = "/api/v1/auth/**";
  public static final String[] GENERIC_URLS = {
          "/",
          "/favicon.ico",
          "/**/*.png",
          "/**/*.gif",
          "/**/*.svg",
          "/**/*.jpg",
          "/**/*.html",
          "/**/*.css",
          "/**/*.js"
  };
  public static final String[] AUTH_WHITELIST = {
          "/swagger-resources/**",
          "/swagger-ui.html",
          "/v2/api-docs",
          "/webjars/**"
  };

}
