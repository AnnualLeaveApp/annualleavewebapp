package com.AnnualLeaveApp.demo.AnnualLeaveApp.data.repo;

import com.AnnualLeaveApp.demo.AnnualLeaveApp.data.model.Application;
import com.AnnualLeaveApp.demo.AnnualLeaveApp.data.model.Person;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.time.LocalDate;
import java.util.Calendar;
import java.util.List;
import java.util.UUID;

public interface ApplicationRepo extends JpaRepository<Application, UUID>{
    @Query("select app from Application app where app.person = ?1 and app.startDate >= ?2 and app.endDate <= ?3 ")
    List<Application> findAllByPersonWithinDateRange(Person person, LocalDate startDate, LocalDate endDate);

    List<Application> findAllByPerson(Person person);

    List<Application> findAllByPersonAndIdNot(Person person, UUID id);
}
