package com.AnnualLeaveApp.demo.AnnualLeaveApp.data.model;

import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.time.LocalDateTime;

@MappedSuperclass
public class BaseSoftDeleteEntity extends BasePrimitiveEntity {

    @Basic
    @Column(name = "is_deleted", columnDefinition = "TINYINT")
    @Type(type = "org.hibernate.type.NumericBooleanType")
    private boolean isDeleted;
    @Basic
    @Column(name = "created_on")
    private LocalDateTime dateCreated;
    @Basic
    @Column(name = "last_modified_on")
    private LocalDateTime dateModified;
    @ManyToOne
    @JoinColumn(name = "created_by", nullable = false)
    private User userCreated;
    @ManyToOne
    @JoinColumn(name = "last_modified_by", nullable = false)
    private User userModified;


    @PrePersist
    protected void onCreate() {
        this.dateCreated = LocalDateTime.now();
        this.dateModified = LocalDateTime.now();
    }

    public boolean isDeleted() {
        return isDeleted;
    }
    public void setDeleted(boolean isDeleted) {
        this.isDeleted = isDeleted;
    }

    @PreUpdate
    protected void onUpdate() {
        this.dateModified = LocalDateTime.now();
    }

    public LocalDateTime getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(LocalDateTime dateCreated) {
        this.dateCreated = dateCreated;
    }

    public LocalDateTime getDateModified() {
        return dateModified;
    }

    public void setDateModified(LocalDateTime dateModified) {
        this.dateModified = dateModified;
    }

    public User getUserCreated() {
        return userCreated;
    }

    public void setUserCreated(User userCreated) {
        this.userCreated = userCreated;
    }

    public User getUserModified() {
        return userModified;
    }

    public void setUserModified(User userModified) {
        this.userModified = userModified;
    }
}
