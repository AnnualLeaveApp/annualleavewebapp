import { FormControl, Validators, AbstractControl } from '@angular/forms';

export class CustomValidators extends Validators {

  static ValidContractDateRanges(dateInputControl: FormControl): any {

    if (dateInputControl.parent == undefined && !dateInputControl.value) {
      return null;
    }

    let startDateInputControl = dateInputControl.parent.get("user_contract_startDateInput");
    let endDateInputControl = dateInputControl.parent.get("user_contract_endDateInput");

    let startDateActivityInput: any;
    let endDateActivityInput: any;

    let searchingByStartDate: Boolean = true;

    if (endDateInputControl == dateInputControl) {
      endDateActivityInput = dateInputControl.value
      startDateActivityInput = startDateInputControl.value;
      searchingByStartDate = false;
    }
    else {
      startDateActivityInput = dateInputControl.value;
      endDateActivityInput = endDateInputControl.value;
    }

    // first check if the controls have values
    if (startDateActivityInput && startDateActivityInput.date && endDateActivityInput && endDateActivityInput.date) {
      let startDate: Date = new Date(startDateActivityInput.date.year, startDateActivityInput.date.month - 1, startDateActivityInput.date.day);
      let endDate: Date = new Date(endDateActivityInput.date.year, endDateActivityInput.date.month - 1, endDateActivityInput.date.day);
      if (startDate > endDate && searchingByStartDate) {
        return { invalidDate: "Start Date is not valid!" };
      }
      else if (startDate > endDate && !searchingByStartDate) {
        return { invalidDate: "End Date is not valid!" };
      }
      else {
        startDateInputControl.setErrors(null);
        endDateInputControl.setErrors(null);
      }
      // if there are matches return an object, else return null.
    } else {
      return null;
    }
  }

  static ValidApplicationDateRanges(dateInputControl: FormControl): any {

    if (dateInputControl.parent == undefined && !dateInputControl.value) {
      return null;
    }

    let startDateInputControl = dateInputControl.parent.get("application_startDateInput");
    let endDateInputControl = dateInputControl.parent.get("application_endDateInput");

    let startDateActivityInput: any;
    let endDateActivityInput: any;

    let searchingByStartDate: Boolean = true;

    if (endDateInputControl == dateInputControl) {
      endDateActivityInput = dateInputControl.value
      startDateActivityInput = startDateInputControl.value;
      searchingByStartDate = false;
    }
    else {
      startDateActivityInput = dateInputControl.value;
      endDateActivityInput = endDateInputControl.value;
    }

    // first check if the controls have values
    if (startDateActivityInput && startDateActivityInput.date && endDateActivityInput && endDateActivityInput.date) {
      let startDate: Date = new Date(startDateActivityInput.date.year, startDateActivityInput.date.month - 1, startDateActivityInput.date.day);
      let endDate: Date = new Date(endDateActivityInput.date.year, endDateActivityInput.date.month - 1, endDateActivityInput.date.day);
      if (startDate > endDate && searchingByStartDate) {
        return { invalidDate: "Start Date is not valid!" };
      }
      else if (startDate > endDate && !searchingByStartDate) {
        return { invalidDate: "End Date is not valid!" };
      }
      else {
        startDateInputControl.setErrors(null);
        endDateInputControl.setErrors(null);
      }
      // if there are matches return an object, else return null.
    } else {
      return null;
    }
  }

  static ValidateEmail(emailInputControl: FormControl): any {

    if (emailInputControl.parent == undefined) {
      return null;
    }

    let emailInput: string = emailInputControl.value as string;

    // first check if the controls have values
    if (emailInput && emailInput.length > 0) {
      let regexp = new RegExp(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/);

      if (!regexp.test(emailInput)) {
        return { invalidEmail: "Invalid email address!" };
      }
      // if there are matches return an object, else return null.
    } else {
      return null;
    }

  }

}


