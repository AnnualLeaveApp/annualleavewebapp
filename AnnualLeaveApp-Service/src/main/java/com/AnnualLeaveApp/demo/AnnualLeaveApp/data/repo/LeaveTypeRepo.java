package com.AnnualLeaveApp.demo.AnnualLeaveApp.data.repo;

import com.AnnualLeaveApp.demo.AnnualLeaveApp.data.model.LeaveType;
import org.springframework.data.jpa.repository.JpaRepository;

public interface LeaveTypeRepo extends JpaRepository<LeaveType, Integer> {
}
