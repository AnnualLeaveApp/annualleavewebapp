package com.AnnualLeaveApp.demo.AnnualLeaveApp.domain.config;


import com.AnnualLeaveApp.demo.AnnualLeaveApp.domain.services.*;
import com.AnnualLeaveApp.demo.AnnualLeaveApp.domain.services.provider.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ServiceBeanConfig {

    @Bean
    public ApplicationProvider applicationProvider() {
        return new ApplicationService();
    }

    @Bean
    public EmailProvider emailProvider() {
        return new EmailService();
    }

    @Bean
    public ContractProvider contractProvider() {
        return new ContractService();
    }

    @Bean
    public RoleProvider roleProvider() {
        return new RoleService();
    }

    @Bean
    public MailTemplateProvider mailTemplateProvider() {
        return new MailTemplateService();
    }

    @Bean
    public UserProvider userProvider() {
        return new UserService();
    }

}
