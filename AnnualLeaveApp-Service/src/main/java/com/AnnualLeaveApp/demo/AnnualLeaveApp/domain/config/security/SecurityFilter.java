package com.AnnualLeaveApp.demo.AnnualLeaveApp.domain.config.security;

import com.AnnualLeaveApp.demo.AnnualLeaveApp.data.model.User;
import com.AnnualLeaveApp.demo.AnnualLeaveApp.domain.services.provider.UserProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.GenericFilterBean;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Optional;

import static java.util.stream.Collectors.toList;

@Component
public class SecurityFilter extends GenericFilterBean {
    @Autowired
    TokenProvider tokenProvider;
    @Autowired
    UserProvider userService;


    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        Optional<String> token = tokenProvider.resolveToken(((HttpServletRequest) request).getHeader("authorization"));
        authenticate(token, request);
        ((HttpServletResponse) response).setHeader("Access-Control-Allow-Origin", "*");
        ((HttpServletResponse) response).setHeader("Access-Control-Allow-Methods", "POST, GET, OPTIONS, DELETE");
        ((HttpServletResponse) response).setHeader("Access-Control-Max-Age", "3600");
        ((HttpServletResponse) response).setHeader("Access-Control-Allow-Headers", "Authorization,Origin, X-Requested-With, Content-Type, Accept");
        chain.doFilter(request, response);
    }

    private void authenticate(Optional<String> token, ServletRequest request) {
        if (!token.isPresent() || !tokenProvider.validateToken(token.get()))
            SecurityContextHolder.getContext().setAuthentication(null);
        else
            authenticateUser(token.get(), request);

    }

    private void authenticateUser(String token, ServletRequest request) {
        SecurityContextHolder.getContext().setAuthentication(getAuthenticationFromUser(getUserFromToken(token), request));
    }

    private Authentication getAuthenticationFromUser(User user, ServletRequest request) {
        addUserAttribute(user, request);
        return user != null ? new UsernamePasswordAuthenticationToken(user,
                "", user.getRoleList().stream()
                .map(s -> new SimpleGrantedAuthority("ROLE_" + s.getName())).collect(toList())) : null;
    }

    private void addUserAttribute(User user, ServletRequest request) {
        if (user != null) request.setAttribute("user", user);
    }

    private User getUserFromToken(String token) {
        return userService.findByUsername(tokenProvider.getUsername(token)).get();
    }
}
