import { Component, OnInit } from '@angular/core';
import { animate, AUTO_STYLE, state, style, transition, trigger } from '@angular/animations';
import { AuthService, UserService } from '../../../../../api/services';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import swal from 'sweetalert2';
import { User, Login } from '../../../../../api/models';
import { FormService } from 'src/app/services/validators/services';
import { NavigationService } from 'src/app/services';



@Component({
  selector: 'app-user-login',
  templateUrl: './user-login.component.html',
  styleUrls: [
    './user-login.component.scss',
    '../../../../../assets/icon/icofont/css/icofont.scss'
  ],
  animations: [
    trigger('notificationBottom', [
      state('an-off, void',
        style({
          overflow: 'hidden',
          height: '0px',
        })
      ),
      state('an-animate',
        style({
          overflow: 'hidden',
          height: AUTO_STYLE,
        })
      ),
      transition('an-off <=> an-animate', [
        animate('400ms ease-in-out')
      ])
    ]),
    trigger('mobileHeaderNavRight', [
      state('nav-off, void',
        style({
          overflow: 'hidden',
          height: '0px',
        })
      ),
      state('nav-on',
        style({
          height: AUTO_STYLE,
        })
      ),
      transition('nav-off <=> nav-on', [
        animate('400ms ease-in-out')
      ])
    ])
  ]
})
export class UserLoginComponent implements OnInit {
  loading = false;
  submitted = false;


  userLoginForm: FormGroup;
  public userLoginFormErrors = {
    user_usernameInput: '',
    user_passwordInput: '',
  };

  public year: number;
  public customerName: string;
  public navType: string;
  public themeLayout: string;
  public verticalPlacement: string;
  public verticalLayout: string;
  public pcodedDeviceType: string;
  public verticalNavType: string;
  public verticalEffect: string;
  public vnavigationView: string;
  public freamType: string;
  public sidebarImg: string;
  public sidebarImgType: string;
  public layoutType: string;

  public headerTheme: string;
  public pcodedHeaderPosition: string;

  public liveNotification: string;
  public liveNotificationClass: string;

  public profileNotification: string;
  public profileNotificationClass: string;

  public searchWidth: number;
  public searchWidthString: string;

  public navRight: string;
  public windowWidth: number;

  constructor(private fb: FormBuilder, private authService: AuthService, private router: Router,
    private FormService: FormService, private navigationService: NavigationService) {

    this.year = new Date().getFullYear();
    this.customerName = "LHIND GmbH";
    this.navType = 'st2';
    this.themeLayout = 'vertical';
    this.verticalPlacement = 'left';
    this.verticalLayout = 'wide';
    this.pcodedDeviceType = 'desktop';
    this.verticalNavType = 'expanded';
    this.verticalEffect = 'shrink';
    this.vnavigationView = 'view1';
    this.freamType = 'theme1';
    this.sidebarImg = 'false';
    this.sidebarImgType = 'img1';
    this.layoutType = 'light';

    this.headerTheme = 'theme1';

    this.liveNotification = 'an-off';
    this.profileNotification = 'an-off';

    this.searchWidth = 0;

    this.navRight = 'nav-on';

    this.windowWidth = window.innerWidth;
    this.setHeaderAttributes(this.windowWidth);
  }

  ngOnInit() {
    document.querySelector('body').setAttribute('themebg-pattern', 'theme1');
    this.initializeForm()
  }

  onResize(event) {
    this.windowWidth = event.target.innerWidth;
    this.setHeaderAttributes(this.windowWidth);
  }

  initializeForm() {

    this.userLoginForm = this.fb.group({
      user_usernameInput:
        [
          '',
          Validators.required
        ],
      user_passwordInput:
        [
          '',
          [Validators.required]
        ]
    });

    this.userLoginForm.valueChanges.subscribe((data) => {
      this.userLoginFormErrors = this.FormService.validateForm(this.userLoginForm, this.userLoginFormErrors, true)
    });
  }

  logInUser() {
    this.submitted = true;

    if (this.userLoginForm.invalid) {
      this.userLoginFormErrors = this.FormService.validateForm(this.userLoginForm, this.userLoginFormErrors, false)
      return;
    }

    this.submitted = false;

    this.loading = true;

    const val = this.userLoginForm.value;

    this.makeLoginCall(val);

}

makeLoginCall(modalFormValues: any){

  let login: Login = {
    username :modalFormValues.user_usernameInput,
    password: modalFormValues.user_passwordInput
  }

  // login.username = modalFormValues.user_usernameInput;
  // login.password = modalFormValues.user_passwordInput;

  this.authService.login(login)
  .subscribe(
    data => {
      this.loading = false;
      localStorage.setItem('userJWT', data.token);
      this.router.navigateByUrl(this.navigationService.getHomePathOnLogin());
    },
    error => {
      this.loading = false;
      if (error.status == 403)
        swal(
          "Unauthorized user",
          "No user exist with the crendentials provided!",
          'error'
        );
    });
}
setHeaderAttributes(windowWidth) {
  if (windowWidth < 992) {
    this.navRight = 'nav-off';
  } else {
    this.navRight = 'nav-on';
  }
}

}