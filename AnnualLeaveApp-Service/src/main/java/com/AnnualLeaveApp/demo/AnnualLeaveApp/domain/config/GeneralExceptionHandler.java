package com.AnnualLeaveApp.demo.AnnualLeaveApp.domain.config;

import com.AnnualLeaveApp.demo.AnnualLeaveApp.data.GeneralException;
import com.AnnualLeaveApp.demo.AnnualLeaveApp.data.model.MyException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@Order(Ordered.HIGHEST_PRECEDENCE)
@ControllerAdvice
public class GeneralExceptionHandler extends ResponseEntityExceptionHandler {
    private static final Logger logger = LogManager.getLogger(GeneralExceptionHandler.class);

    @ExceptionHandler(GeneralException.class)
    protected ResponseEntity<MyException> handleException(GeneralException generalException) {
        logger.error(generalException.toString());
        return ResponseEntity.status(generalException.getStatus()).body(new MyException(generalException.getTitle(), generalException.getMessage()));
    }

}
