package com.AnnualLeaveApp.demo.AnnualLeaveApp.presentation.controller;

import com.AnnualLeaveApp.demo.AnnualLeaveApp.data.dto.swagger.LeaveTypeDTO;
import com.AnnualLeaveApp.demo.AnnualLeaveApp.data.repo.LeaveTypeRepo;
import com.AnnualLeaveApp.demo.AnnualLeaveApp.domain.services.provider.ApplicationProvider;
import com.AnnualLeaveApp.demo.AnnualLeaveApp.domain.util.ObjectMapper;
import com.AnnualLeaveApp.demo.AnnualLeaveApp.presentation.swagger.LeaveTypeApi;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@Controller
@RequestMapping("/api/v1")
public class LeaveTypeController implements LeaveTypeApi {

    @Autowired
    LeaveTypeRepo leaveTypeRepo;

    @Autowired
    HttpServletRequest httpServletRequest;

    @Override
    public Optional<HttpServletRequest> getRequest() {
        return Optional.of(httpServletRequest);
    }

    @Override
    public ResponseEntity<List<LeaveTypeDTO>> findAllTypes() {
        return ResponseEntity.ok(
                leaveTypeRepo
                        .findAll()
                        .stream()
                        .map(ObjectMapper::map)
                        .collect(Collectors.toList()));
    }
}
