import { Injectable } from '@angular/core';
import { JwtHelperService } from '@auth0/angular-jwt';
import { Router } from '@angular/router';
import { NavigationMenu, NavigationTree, NavigationData } from '../models';



@Injectable({ providedIn: 'root' })
export class NavigationService {
    private adminUserHomePagePATH: string = "/admin/users";
    private userHomePagePATH: string = "/user/home";


    private mainMenuSectionLabel: string = "MainMenu";
    private mainMenuSectionIcon_ADMIN: string = "feather icon-users";
    private mainMenuSectionIcon_USER: string = "feather icon-feather";

    //ADMIN Menu Tree
    private adminMainSideBarName: string = "Administration";
    private adminMainViewPath: string = "admin";
    private adminSubSideBarName1: string = "Users";
    private adminSubSideBarViewPath1: string = "users";

    //Other Users Menu Tree
    private userMenuSideBarName: string = "User Data";
    private userMainViewPath: string = "user";
    private userSubSideBarName1: string = "Annual Leaves";
    private userSubSideBarViewPath1: string = "home";

    constructor(private jwtHelper: JwtHelperService, private route: Router) { }

    getHomePathOnLogin() {
        var token = localStorage.getItem('userJWT');
        if (token && !this.jwtHelper.isTokenExpired(token)) {
            let tokenPayload = this.jwtHelper.decodeToken(token)
            let currentUserRoles: string = tokenPayload.roles;
            for (let userRole of currentUserRoles) {
                if (userRole.toUpperCase() == "ADMIN")
                    return this.adminUserHomePagePATH;
            }
            return this.userHomePagePATH;
        }
        else {
            this.logOutUser();
        }
    }

    getUserNameFromToken(): string {
        var token = localStorage.getItem('userJWT');
        if (token && !this.jwtHelper.isTokenExpired(token)) {
            let tokenPayload = this.jwtHelper.decodeToken(token)
            return tokenPayload.username;
        }
        this.logOutUser();
    }

    getUserNavigationMenu(): NavigationMenu[] {

        let navigationMenu: NavigationMenu[] = [];
        var token = localStorage.getItem('userJWT');
        if (token && !this.jwtHelper.isTokenExpired(token)) {
            let tokenPayload = this.jwtHelper.decodeToken(token)
            let currentUserRoles: string[] = tokenPayload.roles;
            if (currentUserRoles && currentUserRoles.length > 0) {
                currentUserRoles.sort();
                for (let userRole of currentUserRoles) {
                    switch (userRole) {
                        case "ADMIN":
                            navigationMenu.push(this.getAdminNavigationMenu());
                            continue;
                        case "USER":
                            navigationMenu.push(this.getOtherUserNavigationMenu());
                            continue;
                        case "SUPERVISOR":
                        case "FINANCE":
                            continue;
                    }
                }
            }
            else {
                this.logOutUser();
            }
            return navigationMenu;
        }
    }

    getAdminNavigationMenu(): NavigationMenu {
        let adminNavigationTree: NavigationTree[] = [];

        let adminNavigationParent: NavigationTree;
        let adminMainMenu: NavigationData;
        let adminSubMenu: NavigationData[] = [];

        adminMainMenu = {
            SideBar: this.adminMainSideBarName,
            View: this.adminMainViewPath,
            IconCSS: this.mainMenuSectionIcon_ADMIN
        };

        let adminSubMenu1: NavigationData;
        adminSubMenu1 = {
            SideBar: this.adminSubSideBarName1,
            View: this.adminSubSideBarViewPath1,
            IconCSS: ""
        };
        adminSubMenu.push(adminSubMenu1);
        adminNavigationParent = {
            MainMenu: adminMainMenu,
            SubMenu: adminSubMenu
        };
        adminNavigationTree.push(adminNavigationParent);

        let adminNavigationMenu: NavigationMenu = {
            Section: this.mainMenuSectionLabel,
            MenuList: adminNavigationTree
        };
        return adminNavigationMenu;
    }

    getOtherUserNavigationMenu(): NavigationMenu {
        let userNavigationTree: NavigationTree[] = [];

        let userNavigationParent: NavigationTree;
        let userMainMenu: NavigationData;
        let userSubMenu: NavigationData[] = [];

        userMainMenu = {
            SideBar: this.userMenuSideBarName,
            View: this.userMainViewPath,
            IconCSS: this.mainMenuSectionIcon_USER
        };

        let userSubMenu1: NavigationData;

        userSubMenu1 = {
            SideBar: this.userSubSideBarName1,
            View: this.userSubSideBarViewPath1,
            IconCSS: ""
        };
        userSubMenu.push(userSubMenu1);
        userNavigationParent = {
            MainMenu: userMainMenu,
            SubMenu: userSubMenu
        };
        userNavigationTree.push(userNavigationParent);

        let userNavigationMenu: NavigationMenu = {
            Section: this.mainMenuSectionLabel,
            MenuList: userNavigationTree
        };
        return userNavigationMenu;
    }

    logOutUser() {
        localStorage.removeItem('userJWT');
        this.route.navigate(['/login']);
    }
}  
