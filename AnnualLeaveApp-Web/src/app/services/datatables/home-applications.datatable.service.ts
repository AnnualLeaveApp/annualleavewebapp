import { DataSource } from '@angular/cdk/table';
import { Application } from '../../../api/models';
import { BehaviorSubject, merge, Observable } from 'rxjs';
import { MatPaginator, MatSort } from '@angular/material';
import { map } from 'rxjs/operators';
import { ApplicationService } from '../../../api/services';

export class ApplicationsDataSource extends DataSource<Application> {

  _filterChange = new BehaviorSubject('');
  dataChange: BehaviorSubject<Application[]> = new BehaviorSubject<Application[]>([]);

  get filter(): string {
    return this._filterChange.value;
  }

  set filter(filter: string) {
    this._filterChange.next(filter);
  }

  filteredData: Application[] = [];
  renderedData: Application[] = [];

  constructor(public _paginator: MatPaginator,
    public _sort: MatSort,
    private userId: string,
    private applicationService: ApplicationService) {
    super();
    // Reset to the first page when the user changes the filter.
    this._filterChange.subscribe(() => this._paginator.pageIndex = 0);
  }

  /** Connect function called by the table to retrieve one stream containing the data to render. */
  connect(): Observable<Application[]> {
    // Listen for any changes in the base data, sorting, filtering, or pagination
    const displayDataChanges = [
      this.dataChange,
      this._sort.sortChange,
      this._filterChange,
      this._paginator.page
    ];

    this.initializeApplicationData();

    return merge(...displayDataChanges).pipe(map(() => {
      // Filter data
      this.filteredData = this.dataChange.value.slice().filter((application: Application) => {
        const searchStr = (application.id + application.applicationStatus).toLowerCase();
        return searchStr.indexOf(this.filter.toLowerCase()) !== -1;
      });

      // Sort filtered data
      const sortedData = this.sortData(this.filteredData.slice());

      // Grab the page's slice of the filtered sorted data.
      const startIndex = this._paginator.pageIndex * this._paginator.pageSize;
      this.renderedData = sortedData.splice(startIndex, this._paginator.pageSize);
      return this.renderedData;
    }
    ));
  }

  initializeApplicationData() {
    if(!this.userId){
      this.applicationService.findAllByUser().subscribe(
        data => {
          this.dataChange.next(data);
        }
      );
    }
  }


  disconnect() { }

  /** Returns a sorted copy of the database data. */
  sortData(data: Application[]): Application[] {
    if (!this._sort.active || this._sort.direction === '') {
      return data;
    }

    return data.sort((a, b) => {
      let propertyA: number | string = '';
      let propertyB: number | string = '';

      switch (this._sort.active) {
        case 'id': [propertyA, propertyB] = [a.id, b.id]; break;
        case 'startDate': [propertyA, propertyB] = [a.startDate, b.startDate]; break;
        case 'endDate': [propertyA, propertyB] = [a.endDate, b.endDate]; break;
        case 'leaveType': [propertyA, propertyB] = [a.leaveTypeId, b.leaveTypeId]; break;
        case 'status': [propertyA, propertyB] = [a.applicationStatus, b.applicationStatus]; break;

      }

      const valueA = isNaN(+propertyA) ? propertyA : +propertyA;
      const valueB = isNaN(+propertyB) ? propertyB : +propertyB;

      return (valueA < valueB ? -1 : 1) * (this._sort.direction === 'asc' ? 1 : -1);
    });
  }
}