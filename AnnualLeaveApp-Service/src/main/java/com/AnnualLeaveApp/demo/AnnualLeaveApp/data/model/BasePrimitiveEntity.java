package com.AnnualLeaveApp.demo.AnnualLeaveApp.data.model;

import java.util.UUID;
import javax.persistence.*;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

@MappedSuperclass
public class BasePrimitiveEntity {
  @Id
  @GeneratedValue(generator = "generalPKGenerator")
  @GenericGenerator(name = "generalPKGenerator", strategy = "org.hibernate.id.UUIDGenerator")
  @Type(type = "uuid-char")
  @Column(name = "id", updatable = false, nullable = false)
  private UUID id;

  public UUID getId() {
    return id;
  }

  public void setId(UUID id) {
    this.id = id;
  }
  
}
