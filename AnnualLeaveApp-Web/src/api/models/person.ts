/* tslint:disable */
export interface Person {
  id?: string;
  firstName?: string;
  lastName?: string;
}
