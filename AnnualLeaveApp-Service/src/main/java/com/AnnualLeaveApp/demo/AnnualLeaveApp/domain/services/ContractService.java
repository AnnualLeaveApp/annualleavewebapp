package com.AnnualLeaveApp.demo.AnnualLeaveApp.domain.services;

import com.AnnualLeaveApp.demo.AnnualLeaveApp.data.ExceptionType;
import com.AnnualLeaveApp.demo.AnnualLeaveApp.data.GeneralException;
import com.AnnualLeaveApp.demo.AnnualLeaveApp.data.model.Contract;
import com.AnnualLeaveApp.demo.AnnualLeaveApp.data.model.Person;
import com.AnnualLeaveApp.demo.AnnualLeaveApp.data.model.User;
import com.AnnualLeaveApp.demo.AnnualLeaveApp.data.repo.ContractRepo;
import com.AnnualLeaveApp.demo.AnnualLeaveApp.data.repo.UserRepo;
import com.AnnualLeaveApp.demo.AnnualLeaveApp.domain.provider.AbstractCrudProvider;
import com.AnnualLeaveApp.demo.AnnualLeaveApp.domain.services.provider.ContractProvider;
import com.AnnualLeaveApp.demo.AnnualLeaveApp.domain.services.provider.PersonProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.sql.Date;
import java.time.LocalDate;
import java.util.Calendar;
import java.util.Optional;
import java.util.UUID;

@Service
public class ContractService extends AbstractCrudProvider<Contract, UUID, ContractRepo> implements ContractProvider {

    @Autowired
    ContractRepo contractRepo;

    @Autowired
    UserRepo userRepo;

    @Override
    protected ContractRepo getRepo() {
        return contractRepo;
    }

    @Override
    public Optional<Contract> getUserContractInfo(UUID userID) {
        Optional<User> user = userRepo.findById(userID);
        if (user.isPresent() && user.get().getPerson() != null) {
            return contractRepo.findValidContractByPerson(user.get().getPerson(), Date.valueOf(LocalDate.now()));
        }
        return Optional.of(new Contract());
    }

    public Contract create(UUID userid, Contract contract, Optional<User> user) {
        Optional<User> existingUser = userRepo.findById(userid);
        if(existingUser.isPresent()){
            contract.setPerson(existingUser.get().getPerson());
            return super.create(contract, user);
        }
        throw new GeneralException(ExceptionType.CONFLICT, "User", "User not found!", userid);
    }

    @Override
    protected String onUpdateObjectNotFoundMessage() {
        return "There is no Contract with this id in our database please try again";
    }
}
