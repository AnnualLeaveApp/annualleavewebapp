INSERT INTO `role`
(`id`,
`name`,
`created_by`,
`created_on`,
`last_modified_by`,
`last_modified_on`)
VALUES
('a3df2a19-ad9c-4d91-9563-082ffa4d9010',
'ADMIN',
'b3df2a19-ad9c-4d91-9563-082ffa4d9009',
'2019-12-25 15:00:00',
'b3df2a19-ad9c-4d91-9563-082ffa4d9009',
'2019-12-25 15:00:00');

INSERT INTO `role`
(`id`,
`name`,
`created_by`,
`created_on`,
`last_modified_by`,
`last_modified_on`)
VALUES
('a3df2a19-ad9c-4d91-9563-082ffa4d9011',
'USER',
'b3df2a19-ad9c-4d91-9563-082ffa4d9009',
'2019-12-25 15:00:00',
'b3df2a19-ad9c-4d91-9563-082ffa4d9009',
'2019-12-25 15:00:00');

INSERT INTO `role`
(`id`,
`name`,
`created_by`,
`created_on`,
`last_modified_by`,
`last_modified_on`)
VALUES
('a3df2a19-ad9c-4d91-9563-082ffa4d9012',
'SUPERVISOR',
'b3df2a19-ad9c-4d91-9563-082ffa4d9009',
'2019-12-25 15:00:00',
'b3df2a19-ad9c-4d91-9563-082ffa4d9009',
'2019-12-25 15:00:00');

INSERT INTO `role`
(`id`,
`name`,
`created_by`,
`created_on`,
`last_modified_by`,
`last_modified_on`)
VALUES
('a3df2a19-ad9c-4d91-9563-082ffa4d9013',
'FINANCE',
'b3df2a19-ad9c-4d91-9563-082ffa4d9009',
'2019-12-25 15:00:00',
'b3df2a19-ad9c-4d91-9563-082ffa4d9009',
'2019-12-25 15:00:00');
