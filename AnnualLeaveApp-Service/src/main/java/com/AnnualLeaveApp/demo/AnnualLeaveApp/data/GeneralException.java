package com.AnnualLeaveApp.demo.AnnualLeaveApp.data;

import org.springframework.http.HttpStatus;

import java.util.stream.Stream;

public class GeneralException extends RuntimeException {
    private HttpStatus status;
    private String title;
    private String message;
    private Object object;
    private String stackTrace;

    public GeneralException(ExceptionType status, String title, String message, Object object) {
        super(message);
        this.title = title;
        this.message = message;
        this.status = status.getStatus();
        this.object = object;
        StringBuilder stringBuilder = new StringBuilder();
        Stream.of(this.getStackTrace()).forEach(stackTraceElement -> stringBuilder.append(stackTraceElement.toString()+"\n"));
        this.stackTrace = stringBuilder.toString();
    }

    public Object getObject() {
        return object;
    }

    public void setObject(Object object) {
        this.object = object;
    }

    public HttpStatus getStatus() {
        return status;
    }

    public void setStatus(HttpStatus status) {
        this.status = status;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return "GeneralException{" +
                "status=" + status +
                ", title='" + title + '\'' +
                ", message='" + message + '\'' +
                ", object=" + object +
                ", stackTrace='" + stackTrace + '\'' +
                '}';
    }
}
