package com.AnnualLeaveApp.demo.AnnualLeaveApp.domain.services;

import com.AnnualLeaveApp.demo.AnnualLeaveApp.data.ExceptionType;
import com.AnnualLeaveApp.demo.AnnualLeaveApp.data.GeneralException;
import com.AnnualLeaveApp.demo.AnnualLeaveApp.data.dto.swagger.RejectRequestDTO;
import com.AnnualLeaveApp.demo.AnnualLeaveApp.data.model.*;
import com.AnnualLeaveApp.demo.AnnualLeaveApp.data.repo.*;
import com.AnnualLeaveApp.demo.AnnualLeaveApp.domain.provider.AbstractCrudProvider;
import com.AnnualLeaveApp.demo.AnnualLeaveApp.domain.services.provider.ApplicationProvider;
import com.AnnualLeaveApp.demo.AnnualLeaveApp.domain.services.provider.EmailProvider;
import com.AnnualLeaveApp.demo.AnnualLeaveApp.domain.services.provider.MailTemplateProvider;
import net.bytebuddy.implementation.bind.annotation.Super;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.stereotype.Service;

import java.sql.Date;
import java.time.LocalDate;
import java.util.Calendar;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.function.Predicate;

@Service
public class ApplicationService extends AbstractCrudProvider<Application, UUID, ApplicationRepo> implements ApplicationProvider {

    @Autowired
    ApplicationRepo applicationRepo;

    @Autowired
    UserRepo userRepo;

    @Autowired
    ContractRepo contractRepo;

    @Autowired
    RoleRepo roleRepo;

    @Autowired
    LeaveTypeRepo leaveTypeRepo;

    @Autowired
    ApplicationStatusRepo applicationStatusRepo;

    @Autowired
    EmailProvider emailProvider;

    @Autowired
    MailTemplateProvider mailTemplateProvider;

    @Override
    protected ApplicationRepo getRepo() {
        return applicationRepo;
    }

    @Override
    protected String onUpdateObjectNotFoundMessage() {
        return null;
    }

    @Override
    public List<Application> getAllPersonAnnualLeaveApplication(Person person) {
        int currentYear = Calendar.getInstance().get(Calendar.YEAR);
        LocalDate startDate = LocalDate.of(currentYear, 1, 1);
        LocalDate endDate = LocalDate.of(currentYear, 12, 31);
        return applicationRepo.findAllByPersonWithinDateRange(person, startDate, endDate);
    }

    @Override
    public Application create(Application application, Optional<User> user) {
        if (checkIfUserHasExceededProbationPeriod(user.get().getPerson())) {
            if (!checkIfApplicationIntersectsOnCreate(application, user.get().getPerson())) {
                Optional<LeaveType> leaveType = getLeaveType(application.getLeaveType().getId());
                if (leaveType.isPresent()) {
                    application.setLeaveType(leaveType.get());
                } else
                    throw new GeneralException(ExceptionType.INVALID_TYPE, "Application", "Leave type not valid", leaveType);
                ApplicationStatus to_be_approved_status = applicationStatusRepo.findByStatus("To Be Approved").get();
                application.setApplicationStatus(to_be_approved_status);
                application.setPerson(user.get().getPerson());
                Application createdApp = super.create(application, user);
                User applicationRequestUser = userRepo.findFirstByPerson(createdApp.getPerson()).get();
                sendSupervisorNotificationEmail(applicationRequestUser, createdApp.getLeaveType(), createdApp.getStartDate(), createdApp.getEndDate());
                return createdApp;
            } else
                throw new GeneralException(ExceptionType.CONFLICT, "Application", "Application intersects!", application);
        } else
            throw new GeneralException(ExceptionType.INVALID_TYPE, "Application", "User Contract not valid", application);

    }

    @Override
    public Optional<Application> update(Application application, UUID id, Optional<User> user) {
        if (!checkIfApplicationIntersectsOnUpdate(application, user.get().getPerson(), id)) {
            Optional<LeaveType> leaveType = getLeaveType(application.getLeaveType().getId());
            if (leaveType.isPresent()) {
                application.setLeaveType(leaveType.get());
            } else
                throw new GeneralException(ExceptionType.INVALID_TYPE, "Invalid type", "Leave type not valid", leaveType);
            ApplicationStatus existingStatus = null;
            Optional<ApplicationStatus> status = applicationStatusRepo.findByStatus(application.getApplicationStatus().getStatus());
            if (status.isPresent()) {
                application.setApplicationStatus(status.get());
            } else {
                existingStatus = applicationRepo.findById(id).get().getApplicationStatus();
                application.setApplicationStatus(existingStatus);
            }
            application.setPerson(user.get().getPerson());
            Optional<Application> updatedApp = super.update(application, id, user);
            if (updatedApp.isPresent()) {
                if (existingStatus == null) {
                    if (!checkIfApplicationStatusIsNotToBeApproved(status.get())) {
                        return Optional.of(updatedApp.get());
                    }
                    User relatedUser = userRepo.findFirstByPerson(updatedApp.get().getPerson()).get();
                    sendUserNotificationEmail(relatedUser, updatedApp.get(), updatedApp.get().getLeaveType(), status.get());
                }
                return Optional.of(updatedApp.get());
            } else return Optional.empty();
        } else
            throw new GeneralException(ExceptionType.CONFLICT, "Application", "Application intersects!", application);
    }

    @Override
    public Optional<Application> approveApplication(UUID applicationId) {
        Optional<ApplicationStatus> applicationStatus = applicationStatusRepo.findByStatus("Approved");
        return applicationStatus.flatMap(status -> updateApplicationStatus(applicationId, status, Optional.empty()));
    }

    @Override
    public Optional<Application> rejectApplication(UUID applicationId, RejectRequestDTO rejectRequestDTO) {
        Optional<ApplicationStatus> applicationStatus = applicationStatusRepo.findByStatus("Rejected");
        if (rejectRequestDTO.getDescription().isEmpty())
            throw new GeneralException(ExceptionType.INVALID_TYPE, "Application", "Description is required!", rejectRequestDTO);
        return updateApplicationStatus(applicationId, applicationStatus.get(), Optional.of(rejectRequestDTO));
    }

    private Optional<Application> updateApplicationStatus(UUID applicationId, ApplicationStatus applicationStatus, Optional<RejectRequestDTO> rejectRequestDTO) {
        Optional<Application> applicationToApprove = applicationRepo.findById(applicationId);
        if (applicationToApprove.isPresent()) {
            Application existingApplication = applicationToApprove.get();
            existingApplication.setApplicationStatus(applicationStatus);
            if (rejectRequestDTO.isPresent()) {
                existingApplication.setDescription(rejectRequestDTO.get().getDescription());
            } else
                existingApplication.setDescription("");
            applicationRepo.save(existingApplication);
            User user = userRepo.findFirstByPerson(existingApplication.getPerson()).get();
            sendUserNotificationEmail(user, existingApplication, existingApplication.getLeaveType(), applicationStatus);
            return Optional.of(existingApplication);
        }
        throw new GeneralException(ExceptionType.DATA_DOES_NOT_EXIST, "Application", "Application not found!", applicationId);
    }

    private Optional<LeaveType> getLeaveType(int leaveTypeId) {
        return leaveTypeRepo.findById(leaveTypeId);
    }

    private boolean checkIfUserHasExceededProbationPeriod(Person loggedPerson) {
        Optional<Contract> userContract = contractRepo.findValidContractByPerson(loggedPerson, Date.valueOf(LocalDate.now()));
        LocalDate userContractStartDate = userContract.get().getActiveFrom().toLocalDate();
        if (userContract.isPresent() && userContractStartDate.isBefore(LocalDate.now().minusDays(90))) {
            return true;
        }
        return false;
    }

    private boolean checkIfApplicationStatusIsNotToBeApproved(ApplicationStatus status) {
        return !status.getStatus().equalsIgnoreCase("To Be Approved");
    }

    private boolean checkIfApplicationIntersectsOnCreate(Application application, Person person) {
        return applicationRepo.findAllByPerson(person).stream()
                .anyMatch(isIntersectingApplicationPredicate(application.getStartDate(), application.getEndDate()));
    }

    private boolean checkIfApplicationIntersectsOnUpdate(Application application, Person person, UUID id) {
        return applicationRepo.findAllByPersonAndIdNot(person, id).stream()
                .anyMatch(isIntersectingApplicationPredicate(application.getStartDate(), application.getEndDate()));
    }

    private Predicate<Application> isIntersectingApplicationPredicate(LocalDate start, LocalDate end) {
        Predicate<Application> intersectingApplication =
                (app) -> isOverlap(app.getStartDate(), app.getEndDate(), start, end);
        return intersectingApplication;
    }

    private boolean isOverlap(LocalDate start1, LocalDate end1, LocalDate start2, LocalDate end2) {
        return (!end1.isBefore(start2) || !end1.isBefore(end2)) && (!start1.isAfter(start2) || !start1.isAfter(end2));
    }

    private void sendUserNotificationEmail(User user, Application application, LeaveType leaveType, ApplicationStatus applicationStatus) {
        MailTemplate mailTemplate = mailTemplateProvider.findByLangAndAndSubject("en", "Application Status Update");
        UserMail userMail = new UserMail();
        userMail.setName(user.getPerson().getFirstName());
        userMail.setLeaveType(leaveType.getType());
        userMail.setApplicationStatus(applicationStatus.getStatus());
        userMail.setStartDate(application.getStartDate());
        userMail.setEndDate(application.getEndDate());
        SimpleMailMessage mailToSend = emailProvider.createUserNotificationMail(user.getEmail(), "lhind.demo@gmail.com", mailTemplate, userMail);
        emailProvider.sendMail(mailToSend);
    }

    private void sendSupervisorNotificationEmail(User user, LeaveType leaveType, LocalDate startDate, LocalDate endDate) {
        Role role = roleRepo.findByName("SUPERVISOR").get();
        Optional<User> supervisorUser = userRepo.findFirstByRoleList(role);
        if (supervisorUser.isPresent()) {
            MailTemplate mailTemplate = mailTemplateProvider.findByLangAndAndSubject("en", "New Leave Request");
            SupervisorMail supervisorMail = new SupervisorMail();
            supervisorMail.setName(supervisorUser.get().getPerson().getFirstName());
            supervisorMail.setUser_name(user.getPerson().getFirstName());
            supervisorMail.setLeaveType(leaveType.getType());
            supervisorMail.setStartDate(startDate);
            supervisorMail.setEndDate(endDate);
            SimpleMailMessage mailToSend = emailProvider.createSupervisorNotificationMail(supervisorUser.get().getEmail(), "lhind.demo@gmail.com", mailTemplate, supervisorMail);
            emailProvider.sendMail(mailToSend);
        }
    }

}
