package com.AnnualLeaveApp.demo.AnnualLeaveApp.data.repo;

import com.AnnualLeaveApp.demo.AnnualLeaveApp.data.model.Person;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;
import java.util.UUID;

public interface PersonRepo extends JpaRepository<Person, UUID> {
    Optional<Person> findByFirstNameAndLastName(String firstName, String lastName);
}
