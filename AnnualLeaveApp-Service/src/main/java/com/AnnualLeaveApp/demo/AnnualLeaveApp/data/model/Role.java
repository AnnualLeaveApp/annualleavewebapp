package com.AnnualLeaveApp.demo.AnnualLeaveApp.data.model;

import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.sql.Timestamp;
import java.util.Objects;

@Entity
@SQLDelete(sql = "UPDATE role SET is_deleted = true WHERE id = ?")
@Where(clause = " is_deleted = 0")
public class Role extends BaseSoftDeleteEntity{
    private String name;

    @Basic
    @Column(name = "name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
