INSERT INTO `person`
(`id`,
`first_name`,
`last_name`,
`created_by`,
`created_on`,
`last_modified_by`,
`last_modified_on`)
VALUES
('a3df2a19-ad9c-4d91-9563-082ffa4d9009',
'Ergis',
'Peti',
'b3df2a19-ad9c-4d91-9563-082ffa4d9009',
'2019-12-25 11:59:59',
'b3df2a19-ad9c-4d91-9563-082ffa4d9009',
'2019-12-25 11:59:59');

INSERT INTO `user`
(`id`,
`username`,
`email`,
`password`,
`person_id`,
`verification_token`,
`created_by`,
`created_on`,
`last_modified_by`,
`last_modified_on`)
VALUES
('b3df2a19-ad9c-4d91-9563-082ffa4d9009',
'epeti',
'petiergis@gmail.com',
'$2y$12$s7M4zOL0E/5KGGxNgKbzKe3aqRJbkFsH1aR0zhpdRx2FjopWY.J6.',
'a3df2a19-ad9c-4d91-9563-082ffa4d9009',
'',
'b3df2a19-ad9c-4d91-9563-082ffa4d9009',
'2019-12-25 12:00:00',
'b3df2a19-ad9c-4d91-9563-082ffa4d9009',
'2019-12-25 12:00:00');
