package com.AnnualLeaveApp.demo.AnnualLeaveApp.data.model;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "application_status", schema = "lhind_demo", catalog = "")
public class ApplicationStatus {
    private int id;
    private String status;

    @Id
    @Column(name = "ID")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "Status")
    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

}
