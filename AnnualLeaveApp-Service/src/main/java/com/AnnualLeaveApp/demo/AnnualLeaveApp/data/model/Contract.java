package com.AnnualLeaveApp.demo.AnnualLeaveApp.data.model;

import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.sql.Date;
import java.sql.Timestamp;
import java.util.Objects;

@Table(name = "contract", schema = "lhind_demo")
@SQLDelete(sql = "UPDATE contract SET is_deleted = 1 WHERE id = ?")
@Where(clause = " is_deleted = 0")
@Entity
public class Contract extends BaseSoftDeleteEntity{
    private int annualLeaveDays;
    private Date activeFrom;
    private Date activeTo;

    @ManyToOne
    @JoinColumn(name = "person_id")
    private Person person;

    @Basic
    @Column(name = "annual_leave_days")
    public int getAnnualLeaveDays() {
        return annualLeaveDays;
    }

    public void setAnnualLeaveDays(int annualLeaveDays) {
        this.annualLeaveDays = annualLeaveDays;
    }

    @Column(name = "active_from")
    public Date getActiveFrom() {
        return activeFrom;
    }

    public void setActiveFrom(Date activeFrom) {
        this.activeFrom = activeFrom;
    }

    @Basic
    @Column(name = "active_to")
    public Date getActiveTo() {
        return activeTo;
    }

    public void setActiveTo(Date activeTo) {
        this.activeTo = activeTo;
    }

    public Person getPerson() {
        return person;
    }

    public void setPerson(Person person) {
        this.person = person;
    }

}
