package com.AnnualLeaveApp.demo.AnnualLeaveApp.domain.provider;

import java.util.List;
import java.util.Optional;

import com.AnnualLeaveApp.demo.AnnualLeaveApp.data.model.User;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Page;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CrudProvider<T, R, E extends JpaRepository<T, R>> {

  List<T> findAll();

  Optional<T> findById(R id);

  T create(T t, Optional<User> user);

  Optional<T> update(T t, R id, Optional<User> user);

  R delete(R id);

  Page<T> findPageable(Pageable pageable);

  boolean exist(R id);

}
