package com.AnnualLeaveApp.demo.AnnualLeaveApp.domain.services.provider;

import com.AnnualLeaveApp.demo.AnnualLeaveApp.data.model.Contract;
import com.AnnualLeaveApp.demo.AnnualLeaveApp.data.model.User;
import com.AnnualLeaveApp.demo.AnnualLeaveApp.data.repo.ContractRepo;
import com.AnnualLeaveApp.demo.AnnualLeaveApp.domain.provider.CrudProvider;

import java.util.Optional;
import java.util.UUID;

public interface ContractProvider extends CrudProvider<Contract, UUID, ContractRepo> {
    Optional<Contract> getUserContractInfo(UUID userId);
}
