import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {AdminComponent} from './layout/admin/admin.component';
import { UserLoginComponent } from './theme/auth/login/user-login/user-login.component';
import { AuthGuard, RoleGuardService as RoleGuard } from './guards';

const routes: Routes = [
  {
    path: '', 
    component: AdminComponent,
    canActivate: [AuthGuard],
    children: [
      {
        path: 'admin',
        loadChildren: './theme/admin/admin.module#AdminModule'
      },
      {
        path: 'user',
        loadChildren: './theme/user/user.module#UserModule'
      },
    ]
  },
  { path: 'login',
   component: UserLoginComponent
   },
  { 
    path: '**',
    redirectTo: '' 
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
