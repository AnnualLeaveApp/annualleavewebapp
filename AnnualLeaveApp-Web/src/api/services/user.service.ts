/* tslint:disable */
import { Injectable } from '@angular/core';
import { HttpClient, HttpRequest, HttpResponse, HttpHeaders } from '@angular/common/http';
import { BaseService as __BaseService } from '../base-service';
import { ApiConfiguration as __Configuration } from '../api-configuration';
import { StrictHttpResponse as __StrictHttpResponse } from '../strict-http-response';
import { Observable as __Observable } from 'rxjs';
import { map as __map, filter as __filter } from 'rxjs/operators';

import { User } from '../models/user';
import { UserRequest } from '../models/user-request';
import { Contract } from '../models/contract';
@Injectable({
  providedIn: 'root',
})
class UserService extends __BaseService {
  static readonly createUserPath = '/user';
  static readonly findAllUsersPath = '/user';
  static readonly updateUserPath = '/user/{userId}';
  static readonly findUserByIdPath = '/user/{userId}';
  static readonly deleteUserPath = '/user/{userId}';
  static readonly findContractOfUserPath = '/user/{userid}/contract';
  static readonly createContractPath = '/user/{userid}/contract';

  constructor(
    config: __Configuration,
    http: HttpClient
  ) {
    super(config, http);
  }

  /**
   * @param userRequest undefined
   * @return OK
   */
  createUserResponse(userRequest: UserRequest): __Observable<__StrictHttpResponse<User>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    __body = userRequest;
    let req = new HttpRequest<any>(
      'POST',
      this.rootUrl + `/user`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<User>;
      })
    );
  }
  /**
   * @param userRequest undefined
   * @return OK
   */
  createUser(userRequest: UserRequest): __Observable<User> {
    return this.createUserResponse(userRequest).pipe(
      __map(_r => _r.body as User)
    );
  }

  /**
   * @return OK
   */
  findAllUsersResponse(): __Observable<__StrictHttpResponse<Array<User>>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/user`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<Array<User>>;
      })
    );
  }
  /**
   * @return OK
   */
  findAllUsers(): __Observable<Array<User>> {
    return this.findAllUsersResponse().pipe(
      __map(_r => _r.body as Array<User>)
    );
  }

  /**
   * @param params The `UserService.UpdateUserParams` containing the following parameters:
   *
   * - `userRequest`:
   *
   * - `userId`:
   *
   * @return OK
   */
  updateUserResponse(params: UserService.UpdateUserParams): __Observable<__StrictHttpResponse<User>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    __body = params.userRequest;

    let req = new HttpRequest<any>(
      'PUT',
      this.rootUrl + `/user/${params.userId}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<User>;
      })
    );
  }
  /**
   * @param params The `UserService.UpdateUserParams` containing the following parameters:
   *
   * - `userRequest`:
   *
   * - `userId`:
   *
   * @return OK
   */
  updateUser(params: UserService.UpdateUserParams): __Observable<User> {
    return this.updateUserResponse(params).pipe(
      __map(_r => _r.body as User)
    );
  }

  /**
   * @param userId undefined
   * @return OK
   */
  findUserByIdResponse(userId: string): __Observable<__StrictHttpResponse<User>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/user/${userId}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<User>;
      })
    );
  }
  /**
   * @param userId undefined
   * @return OK
   */
  findUserById(userId: string): __Observable<User> {
    return this.findUserByIdResponse(userId).pipe(
      __map(_r => _r.body as User)
    );
  }

  /**
   * @param userId undefined
   * @return OK
   */
  deleteUserResponse(userId: string): __Observable<__StrictHttpResponse<string>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    let req = new HttpRequest<any>(
      'DELETE',
      this.rootUrl + `/user/${userId}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'text'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<string>;
      })
    );
  }
  /**
   * @param userId undefined
   * @return OK
   */
  deleteUser(userId: string): __Observable<string> {
    return this.deleteUserResponse(userId).pipe(
      __map(_r => _r.body as string)
    );
  }

  /**
   * @param userid undefined
   * @return OK
   */
  findContractOfUserResponse(userid: string): __Observable<__StrictHttpResponse<Contract>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/user/${userid}/contract`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<Contract>;
      })
    );
  }
  /**
   * @param userid undefined
   * @return OK
   */
  findContractOfUser(userid: string): __Observable<Contract> {
    return this.findContractOfUserResponse(userid).pipe(
      __map(_r => _r.body as Contract)
    );
  }

  /**
   * @param params The `UserService.CreateContractParams` containing the following parameters:
   *
   * - `userid`:
   *
   * - `contract`:
   *
   * @return OK
   */
  createContractResponse(params: UserService.CreateContractParams): __Observable<__StrictHttpResponse<Contract>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    __body = params.contract;
    let req = new HttpRequest<any>(
      'POST',
      this.rootUrl + `/user/${params.userid}/contract`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<Contract>;
      })
    );
  }
  /**
   * @param params The `UserService.CreateContractParams` containing the following parameters:
   *
   * - `userid`:
   *
   * - `contract`:
   *
   * @return OK
   */
  createContract(params: UserService.CreateContractParams): __Observable<Contract> {
    return this.createContractResponse(params).pipe(
      __map(_r => _r.body as Contract)
    );
  }
}

module UserService {

  /**
   * Parameters for updateUser
   */
  export interface UpdateUserParams {
    userRequest: UserRequest;
    userId: string;
  }

  /**
   * Parameters for createContract
   */
  export interface CreateContractParams {
    userid: string;
    contract: Contract;
  }
}

export { UserService }
