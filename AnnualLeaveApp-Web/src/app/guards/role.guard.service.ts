import { Injectable } from '@angular/core';
import { 
  Router,
  CanActivate,
  ActivatedRouteSnapshot
} from '@angular/router';
import { JwtHelperService } from '@auth0/angular-jwt';
import { NavigationService } from '../services/navigation.service';

@Injectable({ providedIn: 'root' })
export class RoleGuardService implements CanActivate {
  constructor(public router: Router, private jwtHelper: JwtHelperService, private navigationService: NavigationService) {}
  canActivate(route: ActivatedRouteSnapshot): boolean {
    // this will be passed from the route config
    // on the data property
    const expectedRole = route.data.expectedRole;
    var token = localStorage.getItem('userJWT');
        if (token && !this.jwtHelper.isTokenExpired(token)) 
        {
            let tokenPayload = this.jwtHelper.decodeToken(token)
            let currentUserRoles = tokenPayload.roles;
            {
              for(let userRole of currentUserRoles)
              {
                if(userRole == expectedRole)
                return true;
              }
            }
            this.router.navigate([this.navigationService.getHomePathOnLogin()])
            return false;
        }
  }
}