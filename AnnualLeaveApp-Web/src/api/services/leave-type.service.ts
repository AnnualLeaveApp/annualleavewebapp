/* tslint:disable */
import { Injectable } from '@angular/core';
import { HttpClient, HttpRequest, HttpResponse, HttpHeaders } from '@angular/common/http';
import { BaseService as __BaseService } from '../base-service';
import { ApiConfiguration as __Configuration } from '../api-configuration';
import { StrictHttpResponse as __StrictHttpResponse } from '../strict-http-response';
import { Observable as __Observable } from 'rxjs';
import { map as __map, filter as __filter } from 'rxjs/operators';

import { LeaveType } from '../models/leave-type';
@Injectable({
  providedIn: 'root',
})
class LeaveTypeService extends __BaseService {
  static readonly findAllTypesPath = '/leaveType';

  constructor(
    config: __Configuration,
    http: HttpClient
  ) {
    super(config, http);
  }

  /**
   * @return OK
   */
  findAllTypesResponse(): __Observable<__StrictHttpResponse<Array<LeaveType>>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/leaveType`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<Array<LeaveType>>;
      })
    );
  }
  /**
   * @return OK
   */
  findAllTypes(): __Observable<Array<LeaveType>> {
    return this.findAllTypesResponse().pipe(
      __map(_r => _r.body as Array<LeaveType>)
    );
  }
}

module LeaveTypeService {
}

export { LeaveTypeService }
