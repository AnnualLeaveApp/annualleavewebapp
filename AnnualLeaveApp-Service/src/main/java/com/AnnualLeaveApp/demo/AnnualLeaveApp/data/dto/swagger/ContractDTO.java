package com.AnnualLeaveApp.demo.AnnualLeaveApp.data.dto.swagger;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.time.LocalDate;
import java.util.UUID;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * ContractDTO
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2020-01-08T12:21:34.454+01:00")

public class ContractDTO   {
  @JsonProperty("id")
  private UUID id = null;

  @JsonProperty("validFrom")
  private LocalDate validFrom = null;

  @JsonProperty("validTo")
  private LocalDate validTo = null;

  @JsonProperty("annualLeaveDays")
  private Integer annualLeaveDays = null;

  public ContractDTO id(UUID id) {
    this.id = id;
    return this;
  }

  /**
   * Get id
   * @return id
  **/
  @ApiModelProperty(value = "")

  @Valid

  public UUID getId() {
    return id;
  }

  public void setId(UUID id) {
    this.id = id;
  }

  public ContractDTO validFrom(LocalDate validFrom) {
    this.validFrom = validFrom;
    return this;
  }

  /**
   * Get validFrom
   * @return validFrom
  **/
  @ApiModelProperty(value = "")

  @Valid

  public LocalDate getValidFrom() {
    return validFrom;
  }

  public void setValidFrom(LocalDate validFrom) {
    this.validFrom = validFrom;
  }

  public ContractDTO validTo(LocalDate validTo) {
    this.validTo = validTo;
    return this;
  }

  /**
   * Get validTo
   * @return validTo
  **/
  @ApiModelProperty(value = "")

  @Valid

  public LocalDate getValidTo() {
    return validTo;
  }

  public void setValidTo(LocalDate validTo) {
    this.validTo = validTo;
  }

  public ContractDTO annualLeaveDays(Integer annualLeaveDays) {
    this.annualLeaveDays = annualLeaveDays;
    return this;
  }

  /**
   * Get annualLeaveDays
   * @return annualLeaveDays
  **/
  @ApiModelProperty(value = "")


  public Integer getAnnualLeaveDays() {
    return annualLeaveDays;
  }

  public void setAnnualLeaveDays(Integer annualLeaveDays) {
    this.annualLeaveDays = annualLeaveDays;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    ContractDTO contract = (ContractDTO) o;
    return Objects.equals(this.id, contract.id) &&
        Objects.equals(this.validFrom, contract.validFrom) &&
        Objects.equals(this.validTo, contract.validTo) &&
        Objects.equals(this.annualLeaveDays, contract.annualLeaveDays);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, validFrom, validTo, annualLeaveDays);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class ContractDTO {\n");
    
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    validFrom: ").append(toIndentedString(validFrom)).append("\n");
    sb.append("    validTo: ").append(toIndentedString(validTo)).append("\n");
    sb.append("    annualLeaveDays: ").append(toIndentedString(annualLeaveDays)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

