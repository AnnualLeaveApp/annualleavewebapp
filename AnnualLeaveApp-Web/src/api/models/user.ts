/* tslint:disable */
import { Person } from './person';
export interface User {
  id?: string;
  person?: Person;
  username?: string;
  email?: string;
  token?: string;
  remainingLeaveDays?: number;
  roles?: Array<string>;
}
