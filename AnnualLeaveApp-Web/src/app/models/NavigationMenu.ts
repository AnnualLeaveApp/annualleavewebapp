
export interface NavigationTree
{
    MainMenu: NavigationData;
    SubMenu: NavigationData[];
}

export interface NavigationMenu
{
    Section: string;
    MenuList: NavigationTree[];
}

export interface NavigationData
{
    SideBar: string;
    View: string;
    IconCSS: string;
}