package com.AnnualLeaveApp.demo.AnnualLeaveApp.presentation.controller;

import com.AnnualLeaveApp.demo.AnnualLeaveApp.data.dto.swagger.ApplicationDTO;
import com.AnnualLeaveApp.demo.AnnualLeaveApp.data.dto.swagger.ContractDTO;
import com.AnnualLeaveApp.demo.AnnualLeaveApp.data.dto.swagger.UserDTO;
import com.AnnualLeaveApp.demo.AnnualLeaveApp.data.dto.swagger.UserRequestDTO;
import com.AnnualLeaveApp.demo.AnnualLeaveApp.domain.services.ContractService;
import com.AnnualLeaveApp.demo.AnnualLeaveApp.domain.services.UserService;
import com.AnnualLeaveApp.demo.AnnualLeaveApp.domain.services.provider.ContractProvider;
import com.AnnualLeaveApp.demo.AnnualLeaveApp.domain.services.provider.RoleProvider;
import com.AnnualLeaveApp.demo.AnnualLeaveApp.domain.services.provider.UserProvider;
import com.AnnualLeaveApp.demo.AnnualLeaveApp.domain.util.ObjectMapper;
import com.AnnualLeaveApp.demo.AnnualLeaveApp.presentation.swagger.UserApi;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@RestController
@Controller
@RequestMapping("/api/v1")
public class UserController implements UserApi {

    @Autowired
    UserService userService;

    @Autowired
    ContractService contractService;

    @Autowired
    HttpServletRequest httpServletRequest;

    @Override
    public Optional<HttpServletRequest> getRequest() {
        return Optional.of(httpServletRequest);
    }

    @Override
    public ResponseEntity<ContractDTO> createContract(UUID userid, @Valid ContractDTO contract) {
        return ResponseEntity.ok(ObjectMapper.map(contractService.create(userid, ObjectMapper.map(contract), ObjectMapper.map(getRequest()))));
    }

    @Override
    public ResponseEntity<UserDTO> createUser(@Valid UserRequestDTO requestDTO) {
        UserDTO userDTO = ObjectMapper.map(userService.create(ObjectMapper.map(requestDTO.getUser()),
                ObjectMapper.map(requestDTO.getContract()), ObjectMapper.map(getRequest()).get()));
        userDTO.setRemainingLeaveDays(userService.getRemainingAnnualLeaveDays(ObjectMapper.map(userDTO.getPerson())));
        return ResponseEntity.ok(userDTO);
    }

    @Override
    public ResponseEntity<UUID> deleteUser(UUID userId) {
        return ResponseEntity.ok((userService.delete(userId)));
    }

    @Override
    public ResponseEntity<List<UserDTO>> findAllUsers() {
        return ResponseEntity.ok(
                userService
                        .findAll()
                        .stream()
                        .map(ObjectMapper::map)
                        .collect(Collectors.toList()));
    }

    @Override
    public ResponseEntity<ContractDTO> findContractOfUser(UUID userid) {
        return ResponseEntity.ok(ObjectMapper.map(contractService.getUserContractInfo(userid).get()));
    }

    @Override
    public ResponseEntity<UserDTO> findUserById(UUID userId) {
        UserDTO userDTO = ObjectMapper.map(userService.findById(userId).get());
        userDTO.setRemainingLeaveDays(userService.getRemainingAnnualLeaveDays(ObjectMapper.map(userDTO.getPerson())));
        return ResponseEntity.ok(userDTO);
    }

    @Override
    public ResponseEntity<UserDTO> updateUser(@Valid UserRequestDTO userRequestDTO, UUID userId) {
        UserDTO userDTO = ObjectMapper.map(userService.update(userId, ObjectMapper.map(userRequestDTO.getUser()),
                ObjectMapper.map(userRequestDTO.getContract()), ObjectMapper.map(getRequest()).get()));
        userDTO.setRemainingLeaveDays(userService.getRemainingAnnualLeaveDays(ObjectMapper.map(userDTO.getPerson())));
        return ResponseEntity.ok(userDTO);
    }
}
