package com.AnnualLeaveApp.demo.AnnualLeaveApp.domain.provider;

import com.AnnualLeaveApp.demo.AnnualLeaveApp.data.ExceptionType;
import com.AnnualLeaveApp.demo.AnnualLeaveApp.data.GeneralException;
import com.AnnualLeaveApp.demo.AnnualLeaveApp.data.model.BaseSoftDeleteEntity;
import com.AnnualLeaveApp.demo.AnnualLeaveApp.data.model.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

public abstract class AbstractCrudProvider<T extends BaseSoftDeleteEntity, R, E extends JpaRepository<T, R>> implements CrudProvider<T, R, E> {
    protected abstract E getRepo();

    protected abstract String onUpdateObjectNotFoundMessage();

    public List<T> findAll() {
        return getRepo().findAll();
    }

    public Optional<T> findById(R id) {
        return getRepo().findById(id);
    }

    public T create(T t, Optional<User> user) {
        setUserCreatedUpdated(t, user);
        setDateCreatedModified(t);
        return getRepo().save(t);
    }

    private void setDateCreatedModified(T t) {
        t.setDateCreated(LocalDateTime.now());
        setDateModified(t);
    }

    private void setUserCreatedUpdated(T t, Optional<User> user) {
        if (user.isPresent()) {
            t.setUserCreated(user.get());
            t.setUserModified(user.get());
        }
    }

    public Optional<T> update(T t, R id, Optional<User> user) {
        setUserUpdated(t, user);
        setDateModified(t);
        Optional<T> record = getRepo().findById(id);
        if(record.isPresent()){
            t.setId(record.get().getId());
            t.setDateCreated(record.get().getDateCreated());
            t.setUserCreated(record.get().getUserCreated());
            return Optional.of(getRepo().save(t));
        }
        throw new GeneralException(ExceptionType.DATA_DOES_NOT_EXIST, "No Data with this Id", onUpdateObjectNotFoundMessage(), t);
    }

    private void setDateModified(T t) {
        t.setDateModified(LocalDateTime.now());
    }

    private void setUserUpdated(T t, Optional<User> user) {
        if (user.isPresent())
            t.setUserModified(user.get());
    }

    public R delete(R id) {
        getRepo().deleteById(id);
        return id;
    }

    public Page<T> findPageable(Pageable pageable) {
        return getRepo().findAll(pageable);
    }

    public boolean exist(R id) {
        return getRepo().existsById(id);
    }
}
