package com.AnnualLeaveApp.demo.AnnualLeaveApp.data;

import org.springframework.http.HttpStatus;

public enum ExceptionType {
    CONFLICT(HttpStatus.CONFLICT), DATA_DOES_NOT_EXIST(HttpStatus.NOT_FOUND), AUTHENTICATION_EXCEPTION(HttpStatus.UNAUTHORIZED),INVALID_TYPE(HttpStatus.BAD_REQUEST);
    private HttpStatus status;

    ExceptionType(HttpStatus status) {
        this.status = status;
    }

    public HttpStatus getStatus() {
        return status;
    }

}
