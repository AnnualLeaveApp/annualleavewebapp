package com.AnnualLeaveApp.demo.AnnualLeaveApp.data.model;

import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Objects;

@Entity
@Table(name = "person", schema = "lhind_demo")
@SQLDelete(sql = "UPDATE person SET is_deleted = 1 WHERE id = ?")
@Where(clause = " is_deleted = 0")
public class Person extends BaseSoftDeleteEntity{
    private String firstName;
    private String lastName;

    @Basic
    @Column(name = "first_name")
    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    @Basic
    @Column(name = "last_name")
    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
}
