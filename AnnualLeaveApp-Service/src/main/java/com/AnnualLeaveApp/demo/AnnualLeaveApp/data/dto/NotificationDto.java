package com.AnnualLeaveApp.demo.AnnualLeaveApp.data.dto;

public class NotificationDto {
    private Object objectToSend;
    private String tableName;
    private String key;

    public Object getObjectToSend() {
        return objectToSend;
    }

    public void setObjectToSend(Object objectToSend) {
        this.objectToSend = objectToSend;
    }

    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }
}
