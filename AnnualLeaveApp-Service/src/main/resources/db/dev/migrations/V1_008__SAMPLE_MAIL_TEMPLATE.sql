INSERT INTO `mail_template`
(`id`,
`subject`,
`message`,
`lang`,
`created_by`,
`created_on`,
`last_modified_by`,
`last_modified_on`)
VALUES
('f3df2a19-ad9c-4d91-9563-082ffa4d9103',
'Application Status Update',
'Hello first_name,

Your leave_type request from start_date to end_date has been application_status!

LHIND DEMO',
'en',
'b3df2a19-ad9c-4d91-9563-082ffa4d9009',
'2019-12-25 15:00:00',
'b3df2a19-ad9c-4d91-9563-082ffa4d9009',
'2019-12-25 15:00:00');

INSERT INTO `mail_template`
(`id`,
`subject`,
`message`,
`lang`,
`created_by`,
`created_on`,
`last_modified_by`,
`last_modified_on`)
VALUES
('f3df2a19-ad9c-4d91-9563-082ffa4d9104',
'New Leave Request',
'Hello supervisor_name,

User user_name has applied for a new leave_type request from start_date to end_date!

LHIND DEMO',
'en',
'b3df2a19-ad9c-4d91-9563-082ffa4d9009',
'2019-12-25 15:00:00',
'b3df2a19-ad9c-4d91-9563-082ffa4d9009',
'2019-12-25 15:00:00');
