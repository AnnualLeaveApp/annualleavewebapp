package com.AnnualLeaveApp.demo.AnnualLeaveApp.domain.services.provider;

import com.AnnualLeaveApp.demo.AnnualLeaveApp.data.model.Person;
import com.AnnualLeaveApp.demo.AnnualLeaveApp.data.repo.PersonRepo;
import com.AnnualLeaveApp.demo.AnnualLeaveApp.domain.provider.CrudProvider;

import java.util.UUID;

public interface PersonProvider extends CrudProvider<Person, UUID, PersonRepo> {
}
