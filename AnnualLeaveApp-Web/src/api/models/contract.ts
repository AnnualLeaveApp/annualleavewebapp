/* tslint:disable */
export interface Contract {
  id?: string;
  validFrom?: string;
  validTo?: string;
  annualLeaveDays?: number;
}
