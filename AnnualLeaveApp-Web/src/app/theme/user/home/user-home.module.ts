import { NgModule } from '@angular/core';
import { CommonModule, registerLocaleData } from '@angular/common';
import {SharedModule} from '../../../shared/shared.module';
import {TagInputModule} from 'ngx-chips';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {DragAndDropModule} from 'angular-draggable-droppable';
import localeIT from '@angular/common/locales/it';
import { CustomValidators } from '../../../services/validators/modal.custom.validators';
import { FormService } from '../../../services/validators/services';
import { DataTablesModule } from 'angular-datatables';
import {
  MatButtonModule, MatDialogModule, MatIconModule, MatInputModule, MatPaginatorModule, MatSortModule,
  MatTableModule, MatToolbarModule, MatDatepickerModule, MatNativeDateModule, MatTooltipModule, MatMenuModule,
} from '@angular/material';
import { MyDatePickerModule } from 'mydatepicker';
import { UserHomeComponent } from './user-home.component';
import { UserHomeRoutingModule } from './user-home-routing.module';

registerLocaleData(localeIT);

@NgModule({
  imports: [
    CommonModule,
    UserHomeRoutingModule,
    SharedModule,
    TagInputModule,
    FormsModule,
    ReactiveFormsModule,
    DragAndDropModule,
    DataTablesModule,
    MatButtonModule,
    MatInputModule,
    MatIconModule,
    MatSortModule,
    MatTableModule,
    MatToolbarModule,
    MatPaginatorModule,
    MatDialogModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatTooltipModule,
    MatIconModule,
    MyDatePickerModule,
    MatMenuModule,
  ],
  exports: [
    DragAndDropModule
  ],
  providers:  [ CustomValidators, FormService],
  declarations: [UserHomeComponent]
})
export class UserHomeModule {}
