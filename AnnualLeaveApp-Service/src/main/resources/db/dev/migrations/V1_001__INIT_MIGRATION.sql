CREATE TABLE `person` (
                          `id` varchar(36) NOT NULL,
                          `first_name` varchar(100) NOT NULL,
                          `last_name` varchar(100) NOT NULL,
                          `is_deleted` boolean DEFAULT false,
                          `created_by` varchar(36) DEFAULT NULL,
                          `created_on` datetime DEFAULT NULL,
                          `last_modified_by` varchar(36) DEFAULT NULL,
                          `last_modified_on` datetime DEFAULT NULL,
                          PRIMARY KEY (`id`)
);


CREATE TABLE `user` (
                        `id` varchar(36) NOT NULL,
                        `username` varchar(45) NOT NULL,
                        `email` varchar(100) NOT NULL,
                        `password` varchar(100) NOT NULL,
                        `person_id` varchar(36) DEFAULT NULL,
                        `verification_token` varchar(45) default NULL,
                        `is_deleted` boolean DEFAULT false,
                        `created_by` varchar(36) DEFAULT NULL,
                        `created_on` datetime DEFAULT NULL,
                        `last_modified_by` varchar(36) DEFAULT NULL,
                        `last_modified_on` datetime DEFAULT NULL,
                        PRIMARY KEY (`id`),
                        KEY `user_person` (`person_id`),
                        CONSTRAINT `user_person` FOREIGN KEY (`person_id`) REFERENCES `person` (`id`)
);

CREATE TABLE `role` (
                        `id` varchar(36) NOT NULL,
                        `name` varchar(45) NOT NULL,
                        `is_deleted` boolean DEFAULT false,
                        `created_by` varchar(36) DEFAULT NULL,
                        `created_on` datetime DEFAULT NULL,
                        `last_modified_by` varchar(36) DEFAULT NULL,
                        `last_modified_on` datetime DEFAULT NULL,
                        PRIMARY KEY (`id`)
);

CREATE TABLE `user_role` (
                             `user_id` varchar(36) NOT NULL,
                             `role_id` varchar(36) NOT NULL,
                             KEY `ur_user_id` (`user_id`),
                             KEY `ur_role_id` (`role_id`),
                             CONSTRAINT `ur_role_id` FOREIGN KEY (`role_id`) REFERENCES `role` (`id`),
                             CONSTRAINT `ur_user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
);

CREATE TABLE `contract` (
                            `id` varchar(36) NOT NULL,
                            `person_id` varchar(36) NOT NULL,
                            `annual_leave_days` int(4) NOT NULL,
                            `is_deleted` boolean DEFAULT false,
                            `active_from` date NOT NULL,
                            `active_to` date DEFAULT NULL,
                            `created_by` varchar(36) DEFAULT NULL,
                            `created_on` datetime DEFAULT NULL,
                            `last_modified_by` varchar(36) DEFAULT NULL,
                            `last_modified_on` datetime DEFAULT NULL,
                            PRIMARY KEY (`id`),
                            KEY `fk_person_id` (`person_id`),
                            CONSTRAINT `fk_person_id` FOREIGN KEY (`person_id`) REFERENCES `person` (`id`)
);

CREATE TABLE `leave_type` (
                              `id` int(11) NOT NULL,
                              `type` varchar(45) NOT NULL,
                              PRIMARY KEY (`id`)
);

CREATE TABLE `application_status` (
                                      `ID` int(11) NOT NULL,
                                      `Status` varchar(45) NOT NULL,
                                      PRIMARY KEY (`ID`)
);

CREATE TABLE `application` (
                               `id` varchar(36) NOT NULL,
                               `person_id` varchar(36) NOT NULL,
                               `start_date` date NOT NULL,
                               `end_date` date NOT NULL,
                               `leave_type_id` int(11) NOT NULL,
                               `application_status_id` int(11) NOT NULL,
                               `description` varchar(1000) DEFAULT NULL,
                               `is_deleted` boolean DEFAULT false,
                               `created_by` varchar(36) DEFAULT NULL,
                               `created_on` datetime DEFAULT NULL,
                               `last_modified_by` varchar(36) DEFAULT NULL,
                               `last_modified_on` datetime DEFAULT NULL,
                               PRIMARY KEY (`ID`),
                               KEY `app_person_id` (`person_id`),
                               KEY `app_leave_type_id` (`leave_type_id`),
                               KEY `app_application_status_id` (`application_status_id`),
                               CONSTRAINT `app_application_status_id` FOREIGN KEY (`application_status_id`) REFERENCES `application_status` (`id`),
                               CONSTRAINT `app_leave_type_id` FOREIGN KEY (`leave_type_id`) REFERENCES `leave_type` (`id`),
                               CONSTRAINT `app_person_id` FOREIGN KEY (`person_id`) REFERENCES `person` (`id`)
);

CREATE TABLE `mail_template` (
                                 `id` varchar(36) NOT NULL,
                                 `subject` varchar(45) DEFAULT NULL,
                                 `message` varchar(1000) DEFAULT NULL,
                                 `lang` varchar(45) DEFAULT NULL,
                                 `is_deleted` boolean DEFAULT false,
                                 `created_by` varchar(36) DEFAULT NULL,
                                 `created_on` datetime DEFAULT NULL,
                                 `last_modified_by` varchar(36) DEFAULT NULL,
                                 `last_modified_on` datetime DEFAULT NULL,
                                 PRIMARY KEY (`ID`)
);
