package com.AnnualLeaveApp.demo.AnnualLeaveApp.data.repo;

import com.AnnualLeaveApp.demo.AnnualLeaveApp.data.model.MailTemplate;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;
import java.util.UUID;

public interface MailTemplateRepo extends JpaRepository<MailTemplate, UUID> {
    Optional<MailTemplate> findByLangAndAndSubject(String lang, String subject);

}
