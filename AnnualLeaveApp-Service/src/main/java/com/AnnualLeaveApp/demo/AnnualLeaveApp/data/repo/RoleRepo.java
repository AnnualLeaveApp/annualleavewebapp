package com.AnnualLeaveApp.demo.AnnualLeaveApp.data.repo;

import com.AnnualLeaveApp.demo.AnnualLeaveApp.data.model.Role;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;
import java.util.UUID;

public interface RoleRepo extends JpaRepository<Role, UUID> {

    Optional<Role> findByName(String role);

}
