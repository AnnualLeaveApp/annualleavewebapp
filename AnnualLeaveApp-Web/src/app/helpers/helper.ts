import { Injectable } from "@angular/core";

@Injectable({ providedIn: 'root' })
export class Helper {

    isValidTime(time: string) {

        var isValid = /^([0-1]?[0-9]|2[0-4]):([0-5][0-9])(:[0-5][0-9])?$/.test(time);
        return isValid;
    }

    parseDateAsUTC(dateString: string) {

        console.log(dateString);
        let date = new Date(dateString + "Z");
        console.log(date);

        let realDate = new Date();
        realDate.setFullYear(date.getUTCFullYear());
        realDate.setMonth(date.getUTCMonth())
        realDate.setHours(date.getUTCHours());
        realDate.setDate(date.getUTCDate());
        realDate.setMinutes(date.getUTCMinutes());
        realDate.setSeconds(0);
        realDate.setMilliseconds(0);

        console.log(realDate);
        return realDate;
    }


    formatDateString(dateToFormat: any) {
        return [dateToFormat.year, (dateToFormat.month > 9 ? '' : '0') + dateToFormat.month, (dateToFormat.day > 9 ? '' : '0') + dateToFormat.day].join('-');
    }

    formatDateTimeFromString(dateTime: string) {
        return new Date(dateTime);
    }

    formatDateStringFromDate(dateToFormat: Date) {
        return [dateToFormat.getFullYear(), (dateToFormat.getMonth() + 1 > 9 ? '' : '0') + (dateToFormat.getMonth() + 1), (dateToFormat.getDate() > 9 ? '' : '0') + dateToFormat.getDate()].join('-');
    }

}